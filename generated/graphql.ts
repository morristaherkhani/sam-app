import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  AWSDate: string;
  AWSDateTime: string;
  AWSEmail: string;
  AWSIPAddress: any;
  AWSJSON: string;
  AWSPhone: any;
  AWSTime: any;
  AWSTimestamp: string;
  AWSURL: string;
  BigInt: any;
  Double: any;
};

export type Appointment = {
  __typename?: 'Appointment';
  appDate: Scalars['AWSDate'];
  appEndTime?: Maybe<Scalars['AWSTime']>;
  appTime: Scalars['AWSTime'];
  appointmentStatus: AppointmentStatus;
  client: Client;
  clientComment?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  selectedService: SelectedService;
  submitionDateTime?: Maybe<Scalars['AWSDateTime']>;
};

export type AppointmentFilter = {
  appDates?: InputMaybe<Array<InputMaybe<Scalars['AWSDate']>>>;
  appintmentStatus?: InputMaybe<AppointmentStatus>;
  id?: InputMaybe<Scalars['ID']>;
  mobile?: InputMaybe<Scalars['ID']>;
};

export type AppointmentInput = {
  address?: InputMaybe<Scalars['String']>;
  allergic?: InputMaybe<Scalars['Boolean']>;
  appDate?: InputMaybe<Scalars['AWSDate']>;
  appTime?: InputMaybe<Scalars['AWSTime']>;
  appointmentStatus?: InputMaybe<AppointmentStatus>;
  clientComment?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  firstName?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  instagram?: InputMaybe<Scalars['String']>;
  lastName?: InputMaybe<Scalars['String']>;
  mobile?: InputMaybe<Scalars['ID']>;
  selectedService?: InputMaybe<SelectedServiceInput>;
  submitionDateTime?: InputMaybe<Scalars['AWSDateTime']>;
};

export type AppointmentOutput = {
  __typename?: 'AppointmentOutput';
  id: Scalars['ID'];
  mobile: Scalars['ID'];
};

export enum AppointmentStatus {
  Cancelled = 'cancelled',
  Confirmed = 'confirmed',
  Done = 'done',
  Entering = 'entering',
  Submitted = 'submitted'
}

export type AuthenticationInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type AuthenticationOutput = {
  __typename?: 'AuthenticationOutput';
  isAuthenticated?: Maybe<Scalars['Boolean']>;
};

export type AvailableTimeInput = {
  date: Scalars['String'];
  serviceDuration: Scalars['Int'];
  serviceMargin: Scalars['Int'];
};

export type AvailableTimeOutput = {
  __typename?: 'AvailableTimeOutput';
  avaliableTimes?: Maybe<Array<Maybe<Scalars['String']>>>;
  firstAvailableTime?: Maybe<FirstAvailableTime>;
};

export type BlockTime = {
  __typename?: 'BlockTime';
  date: Scalars['AWSDate'];
  id: Scalars['ID'];
  time?: Maybe<Scalars['String']>;
  wholeDay?: Maybe<Scalars['Boolean']>;
};

export type BlockTimeInput = {
  date: Scalars['AWSDate'];
  time?: InputMaybe<Scalars['String']>;
  wholeDay?: InputMaybe<Scalars['Boolean']>;
};

export type Client = {
  __typename?: 'Client';
  address?: Maybe<Scalars['String']>;
  allergic?: Maybe<Scalars['Boolean']>;
  askedForFollow?: Maybe<Scalars['Boolean']>;
  askedForReview?: Maybe<Scalars['Boolean']>;
  email?: Maybe<Scalars['String']>;
  firstName: Scalars['String'];
  instagram?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  mobile: Scalars['ID'];
};

export type ClientAppointmentsHistory = {
  __typename?: 'ClientAppointmentsHistory';
  appDate: Scalars['AWSDate'];
  appTime: Scalars['AWSTime'];
  id: Scalars['ID'];
  mobile: Scalars['String'];
  service: Scalars['String'];
};

export type ClientInput = {
  address?: InputMaybe<Scalars['String']>;
  allergic?: InputMaybe<Scalars['Boolean']>;
  email?: InputMaybe<Scalars['String']>;
  firstName: Scalars['String'];
  instagram?: InputMaybe<Scalars['String']>;
  lastName?: InputMaybe<Scalars['String']>;
  mobile: Scalars['ID'];
};

export type ClientReview = {
  __typename?: 'ClientReview';
  isGiven?: Maybe<Scalars['Boolean']>;
  mobile: Scalars['ID'];
  sentDates?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type ClientsForReviewFollow = {
  __typename?: 'ClientsForReviewFollow';
  appointments: Array<ClientAppointmentsHistory>;
  firstName: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  mobile: Scalars['String'];
  sentDates?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type FirstAvailableTime = {
  __typename?: 'FirstAvailableTime';
  avaliableTimes?: Maybe<Array<Maybe<Scalars['String']>>>;
  date?: Maybe<Scalars['AWSDate']>;
};

export type GooglePlace = {
  __typename?: 'GooglePlace';
  rating: Scalars['String'];
  reviews?: Maybe<Array<GoogleReview>>;
  user_ratings_total: Scalars['Int'];
};

export type GooglePlaceInput = {
  rating: Scalars['String'];
  reviews?: InputMaybe<Array<GoogleReviewInput>>;
  user_ratings_total: Scalars['Int'];
};

export type GoogleReview = {
  __typename?: 'GoogleReview';
  author_name: Scalars['String'];
  profile_photo_url: Scalars['String'];
  rating: Scalars['String'];
  relative_time_description: Scalars['String'];
  text: Scalars['String'];
};

export type GoogleReviewInput = {
  author_name: Scalars['String'];
  profile_photo_url: Scalars['String'];
  rating: Scalars['String'];
  relative_time_description: Scalars['String'];
  text: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  addAppointment?: Maybe<AppointmentOutput>;
  addBlockTimes?: Maybe<Array<Maybe<BlockTime>>>;
  addClient?: Maybe<Client>;
  addClientReview?: Maybe<Client>;
  addService?: Maybe<Service>;
  addTeamAppointment?: Maybe<TeamAppointmentOutput>;
  addTeamBlockTimes?: Maybe<Array<Maybe<TeamBlockTime>>>;
  addTeamService?: Maybe<TeamService>;
  addTeamWorkingDays?: Maybe<TeamWorkingDays>;
  addWorkingDays?: Maybe<WorkingDays>;
  confirmCancelAppointment: AppointmentOutput;
  confirmCancelTeamAppointment: TeamAppointmentOutput;
  deleteBlockTimes?: Maybe<Array<Maybe<BlockTime>>>;
  deleteClient?: Maybe<Client>;
  deleteImagesS3Bucket?: Maybe<Array<Maybe<Scalars['String']>>>;
  deleteService?: Maybe<Service>;
  deleteTeamBlockTimes?: Maybe<Array<Maybe<TeamBlockTime>>>;
  updateClientForFollow?: Maybe<Client>;
  updateClientForReview?: Maybe<Client>;
  updateGooglePlace?: Maybe<GooglePlace>;
};


export type MutationAddAppointmentArgs = {
  input?: InputMaybe<AppointmentInput>;
};


export type MutationAddBlockTimesArgs = {
  input: Array<InputMaybe<BlockTimeInput>>;
};


export type MutationAddClientArgs = {
  input?: InputMaybe<ClientInput>;
};


export type MutationAddClientReviewArgs = {
  date: Scalars['String'];
  isGiven: Scalars['Boolean'];
  mobile: Scalars['ID'];
};


export type MutationAddServiceArgs = {
  input: ServiceInput;
};


export type MutationAddTeamAppointmentArgs = {
  input?: InputMaybe<TeamAppointmentInput>;
};


export type MutationAddTeamBlockTimesArgs = {
  input: Array<InputMaybe<TeamBlockTimeInput>>;
};


export type MutationAddTeamServiceArgs = {
  input: ServiceInput;
};


export type MutationAddTeamWorkingDaysArgs = {
  name: Scalars['String'];
  orderDayOfWeek: Scalars['Int'];
  teamCode: Scalars['String'];
  workingDay?: InputMaybe<Scalars['Boolean']>;
  workingHours: Array<InputMaybe<Scalars['String']>>;
};


export type MutationAddWorkingDaysArgs = {
  name: Scalars['String'];
  orderDayOfWeek: Scalars['Int'];
  workingDay?: InputMaybe<Scalars['Boolean']>;
  workingHours: Array<InputMaybe<Scalars['String']>>;
};


export type MutationConfirmCancelAppointmentArgs = {
  appintmentStatus: AppointmentStatus;
  id: Scalars['ID'];
};


export type MutationConfirmCancelTeamAppointmentArgs = {
  appintmentStatus: AppointmentStatus;
  id: Scalars['ID'];
};


export type MutationDeleteBlockTimesArgs = {
  ids?: InputMaybe<Array<Scalars['ID']>>;
};


export type MutationDeleteClientArgs = {
  mobile: Scalars['String'];
};


export type MutationDeleteImagesS3BucketArgs = {
  files: Array<InputMaybe<Scalars['String']>>;
};


export type MutationDeleteServiceArgs = {
  code: Scalars['String'];
};


export type MutationDeleteTeamBlockTimesArgs = {
  ids?: InputMaybe<Array<Scalars['ID']>>;
};


export type MutationUpdateClientForFollowArgs = {
  mobile: Scalars['ID'];
};


export type MutationUpdateClientForReviewArgs = {
  mobile: Scalars['ID'];
};


export type MutationUpdateGooglePlaceArgs = {
  input: GooglePlaceInput;
};

export type PaginationClients = {
  __typename?: 'PaginationClients';
  clients: Array<Client>;
  nextToken?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  authenticate?: Maybe<AuthenticationOutput>;
  getAdmins: Array<Maybe<Scalars['AWSJSON']>>;
  getAppointments?: Maybe<Array<Maybe<Appointment>>>;
  getAvailableTime?: Maybe<AvailableTimeOutput>;
  getBlockTimes?: Maybe<Array<Maybe<BlockTime>>>;
  getClient?: Maybe<Client>;
  getClients?: Maybe<PaginationClients>;
  getClientsForFollow?: Maybe<Array<Maybe<ClientsForReviewFollow>>>;
  getClientsForReview?: Maybe<Array<Maybe<ClientsForReviewFollow>>>;
  getGooglePlace: GooglePlace;
  getImagesS3Bucket?: Maybe<Array<S3File>>;
  getService?: Maybe<Service>;
  getServices?: Maybe<Array<Maybe<Service>>>;
  getTeamAppointments?: Maybe<Array<Maybe<TeamAppointment>>>;
  getTeamAvailableTime?: Maybe<Array<Maybe<TeamAvailableTimeOutput>>>;
  getTeamBlockTimes?: Maybe<Array<Maybe<TeamBlockTime>>>;
  getTeamBlockTimesAll?: Maybe<Array<Maybe<TeamBlockTime>>>;
  getTeamService?: Maybe<TeamService>;
  getTeamServices?: Maybe<Array<Maybe<TeamService>>>;
  getTeamWorkingDays?: Maybe<Array<Maybe<TeamWorkingDays>>>;
  getTeamWorkingDaysTeamCode?: Maybe<Array<Maybe<TeamWorkingDays>>>;
  getWorkingDay?: Maybe<WorkingDays>;
  getWorkingDays?: Maybe<Array<Maybe<WorkingDays>>>;
};


export type QueryAuthenticateArgs = {
  input?: InputMaybe<AuthenticationInput>;
};


export type QueryGetAppointmentsArgs = {
  input?: InputMaybe<AppointmentFilter>;
};


export type QueryGetAvailableTimeArgs = {
  input?: InputMaybe<AvailableTimeInput>;
};


export type QueryGetClientArgs = {
  mobile?: InputMaybe<Scalars['String']>;
};


export type QueryGetClientsArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  nextToken?: InputMaybe<Scalars['String']>;
};


export type QueryGetGooglePlaceArgs = {
  id: Scalars['String'];
};


export type QueryGetImagesS3BucketArgs = {
  s3Path: Scalars['String'];
};


export type QueryGetServiceArgs = {
  code?: InputMaybe<Scalars['String']>;
  requestedFields?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryGetServicesArgs = {
  requestedFields?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryGetTeamAppointmentsArgs = {
  input?: InputMaybe<TeamAppointmentFilter>;
};


export type QueryGetTeamAvailableTimeArgs = {
  input?: InputMaybe<Array<InputMaybe<TeamAvailableTimeInput>>>;
};


export type QueryGetTeamBlockTimesArgs = {
  teamCode: Scalars['String'];
};


export type QueryGetTeamServiceArgs = {
  code?: InputMaybe<Scalars['String']>;
  requestedFields?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryGetTeamServicesArgs = {
  requestedFields?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryGetTeamWorkingDaysArgs = {
  name?: InputMaybe<Scalars['String']>;
  team?: InputMaybe<Scalars['String']>;
};


export type QueryGetTeamWorkingDaysTeamCodeArgs = {
  teamCode: Scalars['String'];
};


export type QueryGetWorkingDayArgs = {
  name: Scalars['String'];
};

export type S3File = {
  __typename?: 'S3File';
  filename: Scalars['String'];
  url: Scalars['String'];
};

export type Schema = {
  __typename?: 'Schema';
  mutation?: Maybe<Mutation>;
  query?: Maybe<Query>;
};

export type SelectedService = {
  __typename?: 'SelectedService';
  service: Service;
  serviceTimePrice: ServiceTimePrice;
};

export type SelectedServiceInput = {
  serviceCode: Scalars['String'];
  serviceTimePriceCode: Scalars['String'];
};

export type Service = {
  __typename?: 'Service';
  code: Scalars['String'];
  displayOrder: Scalars['Int'];
  htmlScripts?: Maybe<Array<Maybe<Scalars['String']>>>;
  images?: Maybe<Array<Maybe<ServiceImages>>>;
  name: Scalars['String'];
  timePrice?: Maybe<Array<Maybe<ServiceTimePrice>>>;
};

export type ServiceImages = {
  __typename?: 'ServiceImages';
  location: Scalars['String'];
  url: Scalars['String'];
};

export type ServiceInput = {
  code: Scalars['String'];
  displayOrder: Scalars['Int'];
  htmlScripts?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  name: Scalars['String'];
  timePrice?: InputMaybe<Array<InputMaybe<ServiceTimePriceInput>>>;
};

export type ServiceTimePrice = {
  __typename?: 'ServiceTimePrice';
  code: Scalars['String'];
  duration: Scalars['Int'];
  margin: Scalars['Int'];
  offer?: Maybe<Scalars['Float']>;
  price: Scalars['Float'];
  title: Scalars['String'];
};

export type ServiceTimePriceInput = {
  code: Scalars['String'];
  duration: Scalars['Int'];
  margin: Scalars['Int'];
  offer?: InputMaybe<Scalars['Float']>;
  price: Scalars['Float'];
  title: Scalars['String'];
};

export type Settings = {
  __typename?: 'Settings';
  settings?: Maybe<Array<Maybe<Scalars['AWSJSON']>>>;
  type: SettingsTypeEnum;
};

export type SettingsInput = {
  settings?: InputMaybe<Array<InputMaybe<Scalars['AWSJSON']>>>;
  type: SettingsTypeEnum;
};

export enum SettingsTypeEnum {
  Admins = 'admins',
  WorkingHours = 'workingHours'
}

export type TeamAppointment = {
  __typename?: 'TeamAppointment';
  appDate: Scalars['AWSDate'];
  appEndTime?: Maybe<Scalars['AWSTime']>;
  appTime: Scalars['AWSTime'];
  appointmentStatus: AppointmentStatus;
  client: Client;
  clientComment?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  selectedService: SelectedService;
  submitionDateTime?: Maybe<Scalars['AWSDateTime']>;
  teamCode: Scalars['String'];
};

export type TeamAppointmentFilter = {
  appDates?: InputMaybe<Array<InputMaybe<Scalars['AWSDate']>>>;
  appintmentStatus?: InputMaybe<AppointmentStatus>;
  id?: InputMaybe<Scalars['ID']>;
  mobile?: InputMaybe<Scalars['ID']>;
  teamCode?: InputMaybe<Scalars['String']>;
};

export type TeamAppointmentInput = {
  address?: InputMaybe<Scalars['String']>;
  allergic?: InputMaybe<Scalars['Boolean']>;
  appDate?: InputMaybe<Scalars['AWSDate']>;
  appTime?: InputMaybe<Scalars['AWSTime']>;
  appointmentStatus?: InputMaybe<AppointmentStatus>;
  clientComment?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  firstName?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  instagram?: InputMaybe<Scalars['String']>;
  lastName?: InputMaybe<Scalars['String']>;
  mobile?: InputMaybe<Scalars['ID']>;
  selectedService?: InputMaybe<SelectedServiceInput>;
  submitionDateTime?: InputMaybe<Scalars['AWSDateTime']>;
  teamCode?: InputMaybe<Scalars['String']>;
};

export type TeamAppointmentOutput = {
  __typename?: 'TeamAppointmentOutput';
  id: Scalars['ID'];
  mobile: Scalars['ID'];
  teamCode: Scalars['String'];
};

export type TeamAvailability = {
  __typename?: 'TeamAvailability';
  dayOfWeek: Scalars['String'];
  teamCode: Scalars['String'];
  workingDay?: Maybe<Scalars['Boolean']>;
  workingHours: Array<Maybe<Scalars['String']>>;
};

export type TeamAvailableTimeInput = {
  date: Scalars['String'];
  duration: Scalars['Int'];
  margin: Scalars['Int'];
  teamCode: Scalars['String'];
};

export type TeamAvailableTimeOutput = {
  __typename?: 'TeamAvailableTimeOutput';
  avaliableTimes?: Maybe<Array<Maybe<Scalars['String']>>>;
  firstAvailableTime?: Maybe<TeamFirstAvailableTime>;
  teamCode?: Maybe<Scalars['String']>;
};

export type TeamBlockTime = {
  __typename?: 'TeamBlockTime';
  date: Scalars['AWSDate'];
  id: Scalars['ID'];
  teamCode: Scalars['String'];
  time?: Maybe<Scalars['String']>;
  wholeDay?: Maybe<Scalars['Boolean']>;
};

export type TeamBlockTimeInput = {
  date: Scalars['AWSDate'];
  teamCode: Scalars['String'];
  time?: InputMaybe<Scalars['String']>;
  wholeDay?: InputMaybe<Scalars['Boolean']>;
};

export type TeamFirstAvailableTime = {
  __typename?: 'TeamFirstAvailableTime';
  avaliableTimes?: Maybe<Array<Maybe<Scalars['String']>>>;
  date?: Maybe<Scalars['AWSDate']>;
  teamCode?: Maybe<Scalars['String']>;
};

export type TeamService = {
  __typename?: 'TeamService';
  code: Scalars['String'];
  displayOrder: Scalars['Int'];
  htmlScripts?: Maybe<Array<Maybe<Scalars['String']>>>;
  images?: Maybe<Array<Maybe<ServiceImages>>>;
  name: Scalars['String'];
  timePrice?: Maybe<Array<Maybe<TeamServiceTimePrice>>>;
};

export type TeamServiceInput = {
  code: Scalars['String'];
  displayOrder: Scalars['Int'];
  htmlScripts?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  name: Scalars['String'];
  timePrice?: InputMaybe<Array<InputMaybe<TeamServiceTimePriceInput>>>;
};

export type TeamServiceTime = {
  __typename?: 'TeamServiceTime';
  duration: Scalars['Int'];
  margin: Scalars['Int'];
  teamCode: Scalars['String'];
};

export type TeamServiceTimeInput = {
  duration: Scalars['Int'];
  margin: Scalars['Int'];
  teamCode: Scalars['String'];
};

export type TeamServiceTimePrice = {
  __typename?: 'TeamServiceTimePrice';
  code: Scalars['String'];
  duration: Scalars['Int'];
  margin: Scalars['Int'];
  offer?: Maybe<Scalars['Float']>;
  price: Scalars['Float'];
  team?: Maybe<Array<Maybe<TeamServiceTime>>>;
  title: Scalars['String'];
};

export type TeamServiceTimePriceInput = {
  code: Scalars['String'];
  duration: Scalars['Int'];
  margin: Scalars['Int'];
  offer?: InputMaybe<Scalars['Float']>;
  price: Scalars['Float'];
  team?: InputMaybe<TeamServiceTimeInput>;
  title: Scalars['String'];
};

export type TeamTimeService = {
  __typename?: 'TeamTimeService';
  duration: Scalars['Int'];
  serviceCode: Scalars['String'];
  teamCode: Scalars['String'];
};

export type TeamWorkingDays = {
  __typename?: 'TeamWorkingDays';
  active?: Maybe<Scalars['Boolean']>;
  name: Scalars['String'];
  orderDayOfWeek: Scalars['Int'];
  teamCode: Scalars['String'];
  workingDay?: Maybe<Scalars['Boolean']>;
  workingHours: Array<Maybe<Scalars['String']>>;
};

export type WorkingDays = {
  __typename?: 'WorkingDays';
  name: Scalars['String'];
  orderDayOfWeek: Scalars['Int'];
  workingDay?: Maybe<Scalars['Boolean']>;
  workingHours: Array<Maybe<Scalars['String']>>;
};

export type ServiceTimePriceFragment = { __typename?: 'ServiceTimePrice', code: string, title: string, price: number, duration: number, margin: number, offer?: number | null };

export type TeamServiceTimeFragment = { __typename?: 'TeamServiceTime', teamCode: string, duration: number, margin: number };

export type TeamServiceTimePriceFragment = { __typename?: 'TeamServiceTimePrice', code: string, title: string, price: number, duration: number, margin: number, offer?: number | null, team?: Array<(
    { __typename?: 'TeamServiceTime' }
    & TeamServiceTimeFragment
  ) | null> | null };

export type ClientAppointmentsHistoryFragment = { __typename?: 'ClientAppointmentsHistory', id: string, appDate: string, appTime: any, mobile: string, service: string };

export type ClientsForReviewFollowFragment = { __typename?: 'ClientsForReviewFollow', firstName: string, lastName?: string | null, mobile: string, sentDates?: Array<string | null> | null, appointments: Array<(
    { __typename?: 'ClientAppointmentsHistory' }
    & ClientAppointmentsHistoryFragment
  )> };

export type ClientFragment = { __typename?: 'Client', mobile: string, firstName: string, lastName?: string | null, instagram?: string | null, email?: string | null, address?: string | null, allergic?: boolean | null };

export type GoogleReviewFragment = { __typename?: 'GoogleReview', author_name: string, profile_photo_url: string, rating: string, relative_time_description: string, text: string };

export type GooglePlaceFragment = { __typename?: 'GooglePlace', user_ratings_total: number, rating: string, reviews?: Array<(
    { __typename?: 'GoogleReview' }
    & GoogleReviewFragment
  )> | null };

export type ServiceImagesFragment = { __typename?: 'ServiceImages', location: string, url: string };

export type ServiceFragment = { __typename?: 'Service', name: string, code: string, htmlScripts?: Array<string | null> | null, displayOrder: number, timePrice?: Array<(
    { __typename?: 'ServiceTimePrice' }
    & ServiceTimePriceFragment
  ) | null> | null, images?: Array<(
    { __typename?: 'ServiceImages' }
    & ServiceImagesFragment
  ) | null> | null };

export type TeamServiceFragment = { __typename?: 'TeamService', name: string, code: string, htmlScripts?: Array<string | null> | null, displayOrder: number, timePrice?: Array<(
    { __typename?: 'TeamServiceTimePrice' }
    & TeamServiceTimePriceFragment
  ) | null> | null, images?: Array<(
    { __typename?: 'ServiceImages' }
    & ServiceImagesFragment
  ) | null> | null };

export type SelectedServiceFragment = { __typename?: 'SelectedService', service: (
    { __typename?: 'Service' }
    & ServiceFragment
  ), serviceTimePrice: (
    { __typename?: 'ServiceTimePrice' }
    & ServiceTimePriceFragment
  ) };

export type FirstAvailableTimeFragment = { __typename?: 'FirstAvailableTime', avaliableTimes?: Array<string | null> | null, date?: string | null };

export type TeamFirstAvailableTimeFragment = { __typename?: 'TeamFirstAvailableTime', avaliableTimes?: Array<string | null> | null, date?: string | null, teamCode?: string | null };

export type AvailableTimeOutputFragment = { __typename?: 'AvailableTimeOutput', avaliableTimes?: Array<string | null> | null, firstAvailableTime?: (
    { __typename?: 'FirstAvailableTime' }
    & FirstAvailableTimeFragment
  ) | null };

export type TeamAvailableTimeOutputFragment = { __typename?: 'TeamAvailableTimeOutput', teamCode?: string | null, avaliableTimes?: Array<string | null> | null, firstAvailableTime?: (
    { __typename?: 'TeamFirstAvailableTime' }
    & TeamFirstAvailableTimeFragment
  ) | null };

export type AppointmentOutputFragment = { __typename?: 'AppointmentOutput', mobile: string, id: string };

export type TeamAppointmentOutputFragment = { __typename?: 'TeamAppointmentOutput', teamCode: string, mobile: string, id: string };

export type AppointmentFragment = { __typename?: 'Appointment', id: string, appDate: string, appTime: any, appEndTime?: any | null, appointmentStatus: AppointmentStatus, clientComment?: string | null, description?: string | null, submitionDateTime?: string | null, selectedService: (
    { __typename?: 'SelectedService' }
    & SelectedServiceFragment
  ), client: (
    { __typename?: 'Client' }
    & ClientFragment
  ) };

export type TeamAppointmentFragment = { __typename?: 'TeamAppointment', id: string, teamCode: string, appDate: string, appTime: any, appEndTime?: any | null, appointmentStatus: AppointmentStatus, clientComment?: string | null, description?: string | null, submitionDateTime?: string | null, selectedService: (
    { __typename?: 'SelectedService' }
    & SelectedServiceFragment
  ), client: (
    { __typename?: 'Client' }
    & ClientFragment
  ) };

export type GetClientQueryVariables = Exact<{
  mobile: Scalars['String'];
}>;


export type GetClientQuery = { __typename?: 'Query', getClient?: (
    { __typename?: 'Client' }
    & ClientFragment
  ) | null };

export type S3FileFragment = { __typename?: 'S3File', url: string, filename: string };

export type GetImagesS3BucketQueryVariables = Exact<{
  s3Path: Scalars['String'];
}>;


export type GetImagesS3BucketQuery = { __typename?: 'Query', getImagesS3Bucket?: Array<(
    { __typename?: 'S3File' }
    & S3FileFragment
  )> | null };

export type GetClientsForReviewQueryVariables = Exact<{ [key: string]: never; }>;


export type GetClientsForReviewQuery = { __typename?: 'Query', getClientsForReview?: Array<(
    { __typename?: 'ClientsForReviewFollow' }
    & ClientsForReviewFollowFragment
  ) | null> | null };

export type GetClientsForFollowQueryVariables = Exact<{ [key: string]: never; }>;


export type GetClientsForFollowQuery = { __typename?: 'Query', getClientsForFollow?: Array<(
    { __typename?: 'ClientsForReviewFollow' }
    & ClientsForReviewFollowFragment
  ) | null> | null };

export type DeleteImagesS3BucketMutationVariables = Exact<{
  files: Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>;
}>;


export type DeleteImagesS3BucketMutation = { __typename?: 'Mutation', deleteImagesS3Bucket?: Array<string | null> | null };

export type GetServicesQueryVariables = Exact<{
  requestedFields?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type GetServicesQuery = { __typename?: 'Query', getServices?: Array<(
    { __typename?: 'Service' }
    & ServiceFragment
  ) | null> | null };

export type GetTeamServicesQueryVariables = Exact<{
  requestedFields?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type GetTeamServicesQuery = { __typename?: 'Query', getTeamServices?: Array<(
    { __typename?: 'TeamService' }
    & TeamServiceFragment
  ) | null> | null };

export type GetServiceQueryVariables = Exact<{
  code?: InputMaybe<Scalars['String']>;
  requestedFields?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type GetServiceQuery = { __typename?: 'Query', getService?: (
    { __typename?: 'Service' }
    & ServiceFragment
  ) | null };

export type GetTeamServiceQueryVariables = Exact<{
  code?: InputMaybe<Scalars['String']>;
  requestedFields?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type GetTeamServiceQuery = { __typename?: 'Query', getTeamService?: (
    { __typename?: 'TeamService' }
    & TeamServiceFragment
  ) | null };

export type AddWorkingDaysMutationVariables = Exact<{
  name: Scalars['String'];
  workingHours: Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>;
  workingDay?: InputMaybe<Scalars['Boolean']>;
  orderDayOfWeek: Scalars['Int'];
}>;


export type AddWorkingDaysMutation = { __typename?: 'Mutation', addWorkingDays?: { __typename?: 'WorkingDays', name: string, workingHours: Array<string | null>, workingDay?: boolean | null, orderDayOfWeek: number } | null };

export type AddTeamWorkingDaysMutationVariables = Exact<{
  teamCode: Scalars['String'];
  name: Scalars['String'];
  workingHours: Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>;
  workingDay?: InputMaybe<Scalars['Boolean']>;
  orderDayOfWeek: Scalars['Int'];
}>;


export type AddTeamWorkingDaysMutation = { __typename?: 'Mutation', addTeamWorkingDays?: { __typename?: 'TeamWorkingDays', teamCode: string, name: string, workingHours: Array<string | null>, workingDay?: boolean | null, orderDayOfWeek: number } | null };

export type GetWorkingDaysQueryVariables = Exact<{ [key: string]: never; }>;


export type GetWorkingDaysQuery = { __typename?: 'Query', getWorkingDays?: Array<{ __typename?: 'WorkingDays', name: string, workingHours: Array<string | null>, workingDay?: boolean | null, orderDayOfWeek: number } | null> | null };

export type GetTeamWorkingDaysQueryVariables = Exact<{ [key: string]: never; }>;


export type GetTeamWorkingDaysQuery = { __typename?: 'Query', getTeamWorkingDays?: Array<{ __typename?: 'TeamWorkingDays', teamCode: string, name: string, workingHours: Array<string | null>, workingDay?: boolean | null, orderDayOfWeek: number, active?: boolean | null } | null> | null };

export type GetTeamWorkingDaysTeamCodeQueryVariables = Exact<{
  teamCode: Scalars['String'];
}>;


export type GetTeamWorkingDaysTeamCodeQuery = { __typename?: 'Query', getTeamWorkingDaysTeamCode?: Array<{ __typename?: 'TeamWorkingDays', teamCode: string, name: string, workingHours: Array<string | null>, workingDay?: boolean | null, orderDayOfWeek: number, active?: boolean | null } | null> | null };

export type GetAvailableTimeQueryVariables = Exact<{
  input: AvailableTimeInput;
}>;


export type GetAvailableTimeQuery = { __typename?: 'Query', getAvailableTime?: (
    { __typename?: 'AvailableTimeOutput' }
    & AvailableTimeOutputFragment
  ) | null };

export type GetTeamAvailableTimeQueryVariables = Exact<{
  input: Array<InputMaybe<TeamAvailableTimeInput>> | InputMaybe<TeamAvailableTimeInput>;
}>;


export type GetTeamAvailableTimeQuery = { __typename?: 'Query', getTeamAvailableTime?: Array<(
    { __typename?: 'TeamAvailableTimeOutput' }
    & TeamAvailableTimeOutputFragment
  ) | null> | null };

export type GetAppointmentsQueryVariables = Exact<{
  input?: InputMaybe<AppointmentFilter>;
}>;


export type GetAppointmentsQuery = { __typename?: 'Query', getAppointments?: Array<(
    { __typename?: 'Appointment' }
    & AppointmentFragment
  ) | null> | null };

export type GetTeamAppointmentsQueryVariables = Exact<{
  input?: InputMaybe<TeamAppointmentFilter>;
}>;


export type GetTeamAppointmentsQuery = { __typename?: 'Query', getTeamAppointments?: Array<(
    { __typename?: 'TeamAppointment' }
    & TeamAppointmentFragment
  ) | null> | null };

export type AddAppointmentMutationVariables = Exact<{
  input?: InputMaybe<AppointmentInput>;
}>;


export type AddAppointmentMutation = { __typename?: 'Mutation', addAppointment?: (
    { __typename?: 'AppointmentOutput' }
    & AppointmentOutputFragment
  ) | null };

export type AddTeamAppointmentMutationVariables = Exact<{
  input?: InputMaybe<TeamAppointmentInput>;
}>;


export type AddTeamAppointmentMutation = { __typename?: 'Mutation', addTeamAppointment?: (
    { __typename?: 'TeamAppointmentOutput' }
    & TeamAppointmentOutputFragment
  ) | null };

export type UpdateGooglePlaceMutationVariables = Exact<{
  input: GooglePlaceInput;
}>;


export type UpdateGooglePlaceMutation = { __typename?: 'Mutation', updateGooglePlace?: (
    { __typename?: 'GooglePlace' }
    & GooglePlaceFragment
  ) | null };

export type GetGooglePlaceQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type GetGooglePlaceQuery = { __typename?: 'Query', getGooglePlace: (
    { __typename?: 'GooglePlace' }
    & GooglePlaceFragment
  ) };

export type AddServiceMutationVariables = Exact<{
  input: ServiceInput;
}>;


export type AddServiceMutation = { __typename?: 'Mutation', addService?: (
    { __typename?: 'Service' }
    & ServiceFragment
  ) | null };

export type ConfirmCancelAppointmentMutationVariables = Exact<{
  id: Scalars['ID'];
  appintmentStatus: AppointmentStatus;
}>;


export type ConfirmCancelAppointmentMutation = { __typename?: 'Mutation', confirmCancelAppointment: (
    { __typename?: 'AppointmentOutput' }
    & AppointmentOutputFragment
  ) };

export type ConfirmCancelTeamAppointmentMutationVariables = Exact<{
  id: Scalars['ID'];
  appintmentStatus: AppointmentStatus;
}>;


export type ConfirmCancelTeamAppointmentMutation = { __typename?: 'Mutation', confirmCancelTeamAppointment: (
    { __typename?: 'TeamAppointmentOutput' }
    & TeamAppointmentOutputFragment
  ) };

export type ClientReviewFragment = { __typename?: 'ClientReview', mobile: string, isGiven?: boolean | null, sentDates?: Array<string | null> | null };

export type AddClientReviewMutationVariables = Exact<{
  mobile: Scalars['ID'];
  date: Scalars['String'];
  isGiven: Scalars['Boolean'];
}>;


export type AddClientReviewMutation = { __typename?: 'Mutation', addClientReview?: (
    { __typename?: 'Client' }
    & ClientFragment
  ) | null };

export type BlockTimeFragment = { __typename?: 'BlockTime', id: string, date: string, time?: string | null, wholeDay?: boolean | null };

export type TeamBlockTimeFragment = { __typename?: 'TeamBlockTime', id: string, date: string, time?: string | null, wholeDay?: boolean | null, teamCode: string };

export type AddBlockTimesMutationVariables = Exact<{
  input: Array<InputMaybe<BlockTimeInput>> | InputMaybe<BlockTimeInput>;
}>;


export type AddBlockTimesMutation = { __typename?: 'Mutation', addBlockTimes?: Array<(
    { __typename?: 'BlockTime' }
    & BlockTimeFragment
  ) | null> | null };

export type AddTeamBlockTimesMutationVariables = Exact<{
  input: Array<InputMaybe<TeamBlockTimeInput>> | InputMaybe<TeamBlockTimeInput>;
}>;


export type AddTeamBlockTimesMutation = { __typename?: 'Mutation', addTeamBlockTimes?: Array<(
    { __typename?: 'TeamBlockTime' }
    & TeamBlockTimeFragment
  ) | null> | null };

export type AddClientMutationVariables = Exact<{
  input: ClientInput;
}>;


export type AddClientMutation = { __typename?: 'Mutation', addClient?: (
    { __typename?: 'Client' }
    & ClientFragment
  ) | null };

export type GetBlockTimesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetBlockTimesQuery = { __typename?: 'Query', getBlockTimes?: Array<(
    { __typename?: 'BlockTime' }
    & BlockTimeFragment
  ) | null> | null };

export type GetTeamBlockTimesQueryVariables = Exact<{
  teamCode: Scalars['String'];
}>;


export type GetTeamBlockTimesQuery = { __typename?: 'Query', getTeamBlockTimes?: Array<(
    { __typename?: 'TeamBlockTime' }
    & TeamBlockTimeFragment
  ) | null> | null };

export type GetTeamBlockTimesAllQueryVariables = Exact<{ [key: string]: never; }>;


export type GetTeamBlockTimesAllQuery = { __typename?: 'Query', getTeamBlockTimesAll?: Array<(
    { __typename?: 'TeamBlockTime' }
    & TeamBlockTimeFragment
  ) | null> | null };

export type DeleteBlockTimesMutationVariables = Exact<{
  ids?: InputMaybe<Array<Scalars['ID']> | Scalars['ID']>;
}>;


export type DeleteBlockTimesMutation = { __typename?: 'Mutation', deleteBlockTimes?: Array<{ __typename?: 'BlockTime', id: string } | null> | null };

export type DeleteTeamBlockTimesMutationVariables = Exact<{
  ids?: InputMaybe<Array<Scalars['ID']> | Scalars['ID']>;
}>;


export type DeleteTeamBlockTimesMutation = { __typename?: 'Mutation', deleteTeamBlockTimes?: Array<{ __typename?: 'TeamBlockTime', id: string } | null> | null };

export type UpdateClientForReviewMutationVariables = Exact<{
  mobile: Scalars['ID'];
}>;


export type UpdateClientForReviewMutation = { __typename?: 'Mutation', updateClientForReview?: (
    { __typename?: 'Client' }
    & ClientFragment
  ) | null };

export type UpdateClientForFollowMutationVariables = Exact<{
  mobile: Scalars['ID'];
}>;


export type UpdateClientForFollowMutation = { __typename?: 'Mutation', updateClientForFollow?: (
    { __typename?: 'Client' }
    & ClientFragment
  ) | null };

export type AuthenticationOutputFragment = { __typename?: 'AuthenticationOutput', isAuthenticated?: boolean | null };

export type AuthenticateQueryVariables = Exact<{
  input?: InputMaybe<AuthenticationInput>;
}>;


export type AuthenticateQuery = { __typename?: 'Query', authenticate?: (
    { __typename?: 'AuthenticationOutput' }
    & AuthenticationOutputFragment
  ) | null };

export const ClientAppointmentsHistoryFragmentDoc = gql`
    fragment ClientAppointmentsHistory on ClientAppointmentsHistory {
  id
  appDate
  appTime
  mobile
  service
}
    `;
export const ClientsForReviewFollowFragmentDoc = gql`
    fragment ClientsForReviewFollow on ClientsForReviewFollow {
  firstName
  lastName
  mobile
  appointments {
    ...ClientAppointmentsHistory
  }
  sentDates
}
    `;
export const GoogleReviewFragmentDoc = gql`
    fragment GoogleReview on GoogleReview {
  author_name
  profile_photo_url
  rating
  relative_time_description
  text
}
    `;
export const GooglePlaceFragmentDoc = gql`
    fragment GooglePlace on GooglePlace {
  reviews {
    ...GoogleReview
  }
  user_ratings_total
  rating
}
    `;
export const TeamServiceTimeFragmentDoc = gql`
    fragment TeamServiceTime on TeamServiceTime {
  teamCode
  duration
  margin
}
    `;
export const TeamServiceTimePriceFragmentDoc = gql`
    fragment TeamServiceTimePrice on TeamServiceTimePrice {
  code
  title
  price
  duration
  margin
  offer
  team {
    ...TeamServiceTime
  }
}
    `;
export const ServiceImagesFragmentDoc = gql`
    fragment ServiceImages on ServiceImages {
  location
  url
}
    `;
export const TeamServiceFragmentDoc = gql`
    fragment TeamService on TeamService {
  name
  code
  timePrice {
    ...TeamServiceTimePrice
  }
  htmlScripts
  images {
    ...ServiceImages
  }
  displayOrder
}
    `;
export const FirstAvailableTimeFragmentDoc = gql`
    fragment FirstAvailableTime on FirstAvailableTime {
  avaliableTimes
  date
}
    `;
export const AvailableTimeOutputFragmentDoc = gql`
    fragment AvailableTimeOutput on AvailableTimeOutput {
  avaliableTimes
  firstAvailableTime {
    ...FirstAvailableTime
  }
}
    `;
export const TeamFirstAvailableTimeFragmentDoc = gql`
    fragment TeamFirstAvailableTime on TeamFirstAvailableTime {
  avaliableTimes
  date
  teamCode
}
    `;
export const TeamAvailableTimeOutputFragmentDoc = gql`
    fragment TeamAvailableTimeOutput on TeamAvailableTimeOutput {
  teamCode
  avaliableTimes
  firstAvailableTime {
    ...TeamFirstAvailableTime
  }
}
    `;
export const AppointmentOutputFragmentDoc = gql`
    fragment AppointmentOutput on AppointmentOutput {
  mobile
  id
}
    `;
export const TeamAppointmentOutputFragmentDoc = gql`
    fragment TeamAppointmentOutput on TeamAppointmentOutput {
  teamCode
  mobile
  id
}
    `;
export const ServiceTimePriceFragmentDoc = gql`
    fragment ServiceTimePrice on ServiceTimePrice {
  code
  title
  price
  duration
  margin
  offer
}
    `;
export const ServiceFragmentDoc = gql`
    fragment Service on Service {
  name
  code
  timePrice {
    ...ServiceTimePrice
  }
  htmlScripts
  images {
    ...ServiceImages
  }
  displayOrder
}
    `;
export const SelectedServiceFragmentDoc = gql`
    fragment SelectedService on SelectedService {
  service {
    ...Service
  }
  serviceTimePrice {
    ...ServiceTimePrice
  }
}
    `;
export const ClientFragmentDoc = gql`
    fragment Client on Client {
  mobile
  firstName
  lastName
  instagram
  email
  address
  allergic
}
    `;
export const AppointmentFragmentDoc = gql`
    fragment Appointment on Appointment {
  id
  selectedService {
    ...SelectedService
  }
  appDate
  appTime
  appEndTime
  appointmentStatus
  clientComment
  description
  client {
    ...Client
  }
  submitionDateTime
}
    `;
export const TeamAppointmentFragmentDoc = gql`
    fragment TeamAppointment on TeamAppointment {
  id
  teamCode
  selectedService {
    ...SelectedService
  }
  appDate
  appTime
  appEndTime
  appointmentStatus
  clientComment
  description
  client {
    ...Client
  }
  submitionDateTime
}
    `;
export const S3FileFragmentDoc = gql`
    fragment S3File on S3File {
  url
  filename
}
    `;
export const ClientReviewFragmentDoc = gql`
    fragment ClientReview on ClientReview {
  mobile
  isGiven
  sentDates
}
    `;
export const BlockTimeFragmentDoc = gql`
    fragment BlockTime on BlockTime {
  id
  date
  time
  wholeDay
}
    `;
export const TeamBlockTimeFragmentDoc = gql`
    fragment TeamBlockTime on TeamBlockTime {
  id
  date
  time
  wholeDay
  teamCode
}
    `;
export const AuthenticationOutputFragmentDoc = gql`
    fragment AuthenticationOutput on AuthenticationOutput {
  isAuthenticated
}
    `;
export const GetClientDocument = gql`
    query GetClient($mobile: String!) {
  getClient(mobile: $mobile) {
    ...Client
  }
}
    ${ClientFragmentDoc}`;

/**
 * __useGetClientQuery__
 *
 * To run a query within a React component, call `useGetClientQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetClientQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetClientQuery({
 *   variables: {
 *      mobile: // value for 'mobile'
 *   },
 * });
 */
export function useGetClientQuery(baseOptions: Apollo.QueryHookOptions<GetClientQuery, GetClientQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetClientQuery, GetClientQueryVariables>(GetClientDocument, options);
      }
export function useGetClientLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetClientQuery, GetClientQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetClientQuery, GetClientQueryVariables>(GetClientDocument, options);
        }
export type GetClientQueryHookResult = ReturnType<typeof useGetClientQuery>;
export type GetClientLazyQueryHookResult = ReturnType<typeof useGetClientLazyQuery>;
export type GetClientQueryResult = Apollo.QueryResult<GetClientQuery, GetClientQueryVariables>;
export const GetImagesS3BucketDocument = gql`
    query GetImagesS3Bucket($s3Path: String!) {
  getImagesS3Bucket(s3Path: $s3Path) {
    ...S3File
  }
}
    ${S3FileFragmentDoc}`;

/**
 * __useGetImagesS3BucketQuery__
 *
 * To run a query within a React component, call `useGetImagesS3BucketQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetImagesS3BucketQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetImagesS3BucketQuery({
 *   variables: {
 *      s3Path: // value for 's3Path'
 *   },
 * });
 */
export function useGetImagesS3BucketQuery(baseOptions: Apollo.QueryHookOptions<GetImagesS3BucketQuery, GetImagesS3BucketQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetImagesS3BucketQuery, GetImagesS3BucketQueryVariables>(GetImagesS3BucketDocument, options);
      }
export function useGetImagesS3BucketLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetImagesS3BucketQuery, GetImagesS3BucketQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetImagesS3BucketQuery, GetImagesS3BucketQueryVariables>(GetImagesS3BucketDocument, options);
        }
export type GetImagesS3BucketQueryHookResult = ReturnType<typeof useGetImagesS3BucketQuery>;
export type GetImagesS3BucketLazyQueryHookResult = ReturnType<typeof useGetImagesS3BucketLazyQuery>;
export type GetImagesS3BucketQueryResult = Apollo.QueryResult<GetImagesS3BucketQuery, GetImagesS3BucketQueryVariables>;
export const GetClientsForReviewDocument = gql`
    query GetClientsForReview {
  getClientsForReview {
    ...ClientsForReviewFollow
  }
}
    ${ClientsForReviewFollowFragmentDoc}
${ClientAppointmentsHistoryFragmentDoc}`;

/**
 * __useGetClientsForReviewQuery__
 *
 * To run a query within a React component, call `useGetClientsForReviewQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetClientsForReviewQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetClientsForReviewQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetClientsForReviewQuery(baseOptions?: Apollo.QueryHookOptions<GetClientsForReviewQuery, GetClientsForReviewQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetClientsForReviewQuery, GetClientsForReviewQueryVariables>(GetClientsForReviewDocument, options);
      }
export function useGetClientsForReviewLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetClientsForReviewQuery, GetClientsForReviewQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetClientsForReviewQuery, GetClientsForReviewQueryVariables>(GetClientsForReviewDocument, options);
        }
export type GetClientsForReviewQueryHookResult = ReturnType<typeof useGetClientsForReviewQuery>;
export type GetClientsForReviewLazyQueryHookResult = ReturnType<typeof useGetClientsForReviewLazyQuery>;
export type GetClientsForReviewQueryResult = Apollo.QueryResult<GetClientsForReviewQuery, GetClientsForReviewQueryVariables>;
export const GetClientsForFollowDocument = gql`
    query GetClientsForFollow {
  getClientsForFollow {
    ...ClientsForReviewFollow
  }
}
    ${ClientsForReviewFollowFragmentDoc}
${ClientAppointmentsHistoryFragmentDoc}`;

/**
 * __useGetClientsForFollowQuery__
 *
 * To run a query within a React component, call `useGetClientsForFollowQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetClientsForFollowQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetClientsForFollowQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetClientsForFollowQuery(baseOptions?: Apollo.QueryHookOptions<GetClientsForFollowQuery, GetClientsForFollowQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetClientsForFollowQuery, GetClientsForFollowQueryVariables>(GetClientsForFollowDocument, options);
      }
export function useGetClientsForFollowLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetClientsForFollowQuery, GetClientsForFollowQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetClientsForFollowQuery, GetClientsForFollowQueryVariables>(GetClientsForFollowDocument, options);
        }
export type GetClientsForFollowQueryHookResult = ReturnType<typeof useGetClientsForFollowQuery>;
export type GetClientsForFollowLazyQueryHookResult = ReturnType<typeof useGetClientsForFollowLazyQuery>;
export type GetClientsForFollowQueryResult = Apollo.QueryResult<GetClientsForFollowQuery, GetClientsForFollowQueryVariables>;
export const DeleteImagesS3BucketDocument = gql`
    mutation DeleteImagesS3Bucket($files: [String]!) {
  deleteImagesS3Bucket(files: $files)
}
    `;
export type DeleteImagesS3BucketMutationFn = Apollo.MutationFunction<DeleteImagesS3BucketMutation, DeleteImagesS3BucketMutationVariables>;

/**
 * __useDeleteImagesS3BucketMutation__
 *
 * To run a mutation, you first call `useDeleteImagesS3BucketMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteImagesS3BucketMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteImagesS3BucketMutation, { data, loading, error }] = useDeleteImagesS3BucketMutation({
 *   variables: {
 *      files: // value for 'files'
 *   },
 * });
 */
export function useDeleteImagesS3BucketMutation(baseOptions?: Apollo.MutationHookOptions<DeleteImagesS3BucketMutation, DeleteImagesS3BucketMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteImagesS3BucketMutation, DeleteImagesS3BucketMutationVariables>(DeleteImagesS3BucketDocument, options);
      }
export type DeleteImagesS3BucketMutationHookResult = ReturnType<typeof useDeleteImagesS3BucketMutation>;
export type DeleteImagesS3BucketMutationResult = Apollo.MutationResult<DeleteImagesS3BucketMutation>;
export type DeleteImagesS3BucketMutationOptions = Apollo.BaseMutationOptions<DeleteImagesS3BucketMutation, DeleteImagesS3BucketMutationVariables>;
export const GetServicesDocument = gql`
    query GetServices($requestedFields: [String]) {
  getServices(requestedFields: $requestedFields) {
    ...Service
  }
}
    ${ServiceFragmentDoc}
${ServiceTimePriceFragmentDoc}
${ServiceImagesFragmentDoc}`;

/**
 * __useGetServicesQuery__
 *
 * To run a query within a React component, call `useGetServicesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetServicesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetServicesQuery({
 *   variables: {
 *      requestedFields: // value for 'requestedFields'
 *   },
 * });
 */
export function useGetServicesQuery(baseOptions?: Apollo.QueryHookOptions<GetServicesQuery, GetServicesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetServicesQuery, GetServicesQueryVariables>(GetServicesDocument, options);
      }
export function useGetServicesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetServicesQuery, GetServicesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetServicesQuery, GetServicesQueryVariables>(GetServicesDocument, options);
        }
export type GetServicesQueryHookResult = ReturnType<typeof useGetServicesQuery>;
export type GetServicesLazyQueryHookResult = ReturnType<typeof useGetServicesLazyQuery>;
export type GetServicesQueryResult = Apollo.QueryResult<GetServicesQuery, GetServicesQueryVariables>;
export const GetTeamServicesDocument = gql`
    query GetTeamServices($requestedFields: [String]) {
  getTeamServices(requestedFields: $requestedFields) {
    ...TeamService
  }
}
    ${TeamServiceFragmentDoc}
${TeamServiceTimePriceFragmentDoc}
${TeamServiceTimeFragmentDoc}
${ServiceImagesFragmentDoc}`;

/**
 * __useGetTeamServicesQuery__
 *
 * To run a query within a React component, call `useGetTeamServicesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamServicesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamServicesQuery({
 *   variables: {
 *      requestedFields: // value for 'requestedFields'
 *   },
 * });
 */
export function useGetTeamServicesQuery(baseOptions?: Apollo.QueryHookOptions<GetTeamServicesQuery, GetTeamServicesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamServicesQuery, GetTeamServicesQueryVariables>(GetTeamServicesDocument, options);
      }
export function useGetTeamServicesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamServicesQuery, GetTeamServicesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamServicesQuery, GetTeamServicesQueryVariables>(GetTeamServicesDocument, options);
        }
export type GetTeamServicesQueryHookResult = ReturnType<typeof useGetTeamServicesQuery>;
export type GetTeamServicesLazyQueryHookResult = ReturnType<typeof useGetTeamServicesLazyQuery>;
export type GetTeamServicesQueryResult = Apollo.QueryResult<GetTeamServicesQuery, GetTeamServicesQueryVariables>;
export const GetServiceDocument = gql`
    query GetService($code: String, $requestedFields: [String]) {
  getService(code: $code, requestedFields: $requestedFields) {
    ...Service
  }
}
    ${ServiceFragmentDoc}
${ServiceTimePriceFragmentDoc}
${ServiceImagesFragmentDoc}`;

/**
 * __useGetServiceQuery__
 *
 * To run a query within a React component, call `useGetServiceQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetServiceQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetServiceQuery({
 *   variables: {
 *      code: // value for 'code'
 *      requestedFields: // value for 'requestedFields'
 *   },
 * });
 */
export function useGetServiceQuery(baseOptions?: Apollo.QueryHookOptions<GetServiceQuery, GetServiceQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetServiceQuery, GetServiceQueryVariables>(GetServiceDocument, options);
      }
export function useGetServiceLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetServiceQuery, GetServiceQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetServiceQuery, GetServiceQueryVariables>(GetServiceDocument, options);
        }
export type GetServiceQueryHookResult = ReturnType<typeof useGetServiceQuery>;
export type GetServiceLazyQueryHookResult = ReturnType<typeof useGetServiceLazyQuery>;
export type GetServiceQueryResult = Apollo.QueryResult<GetServiceQuery, GetServiceQueryVariables>;
export const GetTeamServiceDocument = gql`
    query GetTeamService($code: String, $requestedFields: [String]) {
  getTeamService(code: $code, requestedFields: $requestedFields) {
    ...TeamService
  }
}
    ${TeamServiceFragmentDoc}
${TeamServiceTimePriceFragmentDoc}
${TeamServiceTimeFragmentDoc}
${ServiceImagesFragmentDoc}`;

/**
 * __useGetTeamServiceQuery__
 *
 * To run a query within a React component, call `useGetTeamServiceQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamServiceQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamServiceQuery({
 *   variables: {
 *      code: // value for 'code'
 *      requestedFields: // value for 'requestedFields'
 *   },
 * });
 */
export function useGetTeamServiceQuery(baseOptions?: Apollo.QueryHookOptions<GetTeamServiceQuery, GetTeamServiceQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamServiceQuery, GetTeamServiceQueryVariables>(GetTeamServiceDocument, options);
      }
export function useGetTeamServiceLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamServiceQuery, GetTeamServiceQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamServiceQuery, GetTeamServiceQueryVariables>(GetTeamServiceDocument, options);
        }
export type GetTeamServiceQueryHookResult = ReturnType<typeof useGetTeamServiceQuery>;
export type GetTeamServiceLazyQueryHookResult = ReturnType<typeof useGetTeamServiceLazyQuery>;
export type GetTeamServiceQueryResult = Apollo.QueryResult<GetTeamServiceQuery, GetTeamServiceQueryVariables>;
export const AddWorkingDaysDocument = gql`
    mutation AddWorkingDays($name: String!, $workingHours: [String]!, $workingDay: Boolean, $orderDayOfWeek: Int!) {
  addWorkingDays(
    name: $name
    workingHours: $workingHours
    workingDay: $workingDay
    orderDayOfWeek: $orderDayOfWeek
  ) {
    name
    workingHours
    workingDay
    orderDayOfWeek
  }
}
    `;
export type AddWorkingDaysMutationFn = Apollo.MutationFunction<AddWorkingDaysMutation, AddWorkingDaysMutationVariables>;

/**
 * __useAddWorkingDaysMutation__
 *
 * To run a mutation, you first call `useAddWorkingDaysMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddWorkingDaysMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addWorkingDaysMutation, { data, loading, error }] = useAddWorkingDaysMutation({
 *   variables: {
 *      name: // value for 'name'
 *      workingHours: // value for 'workingHours'
 *      workingDay: // value for 'workingDay'
 *      orderDayOfWeek: // value for 'orderDayOfWeek'
 *   },
 * });
 */
export function useAddWorkingDaysMutation(baseOptions?: Apollo.MutationHookOptions<AddWorkingDaysMutation, AddWorkingDaysMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddWorkingDaysMutation, AddWorkingDaysMutationVariables>(AddWorkingDaysDocument, options);
      }
export type AddWorkingDaysMutationHookResult = ReturnType<typeof useAddWorkingDaysMutation>;
export type AddWorkingDaysMutationResult = Apollo.MutationResult<AddWorkingDaysMutation>;
export type AddWorkingDaysMutationOptions = Apollo.BaseMutationOptions<AddWorkingDaysMutation, AddWorkingDaysMutationVariables>;
export const AddTeamWorkingDaysDocument = gql`
    mutation AddTeamWorkingDays($teamCode: String!, $name: String!, $workingHours: [String]!, $workingDay: Boolean, $orderDayOfWeek: Int!) {
  addTeamWorkingDays(
    teamCode: $teamCode
    name: $name
    workingHours: $workingHours
    workingDay: $workingDay
    orderDayOfWeek: $orderDayOfWeek
  ) {
    teamCode
    name
    workingHours
    workingDay
    orderDayOfWeek
  }
}
    `;
export type AddTeamWorkingDaysMutationFn = Apollo.MutationFunction<AddTeamWorkingDaysMutation, AddTeamWorkingDaysMutationVariables>;

/**
 * __useAddTeamWorkingDaysMutation__
 *
 * To run a mutation, you first call `useAddTeamWorkingDaysMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddTeamWorkingDaysMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addTeamWorkingDaysMutation, { data, loading, error }] = useAddTeamWorkingDaysMutation({
 *   variables: {
 *      teamCode: // value for 'teamCode'
 *      name: // value for 'name'
 *      workingHours: // value for 'workingHours'
 *      workingDay: // value for 'workingDay'
 *      orderDayOfWeek: // value for 'orderDayOfWeek'
 *   },
 * });
 */
export function useAddTeamWorkingDaysMutation(baseOptions?: Apollo.MutationHookOptions<AddTeamWorkingDaysMutation, AddTeamWorkingDaysMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddTeamWorkingDaysMutation, AddTeamWorkingDaysMutationVariables>(AddTeamWorkingDaysDocument, options);
      }
export type AddTeamWorkingDaysMutationHookResult = ReturnType<typeof useAddTeamWorkingDaysMutation>;
export type AddTeamWorkingDaysMutationResult = Apollo.MutationResult<AddTeamWorkingDaysMutation>;
export type AddTeamWorkingDaysMutationOptions = Apollo.BaseMutationOptions<AddTeamWorkingDaysMutation, AddTeamWorkingDaysMutationVariables>;
export const GetWorkingDaysDocument = gql`
    query GetWorkingDays {
  getWorkingDays {
    name
    workingHours
    workingDay
    orderDayOfWeek
  }
}
    `;

/**
 * __useGetWorkingDaysQuery__
 *
 * To run a query within a React component, call `useGetWorkingDaysQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetWorkingDaysQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetWorkingDaysQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetWorkingDaysQuery(baseOptions?: Apollo.QueryHookOptions<GetWorkingDaysQuery, GetWorkingDaysQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetWorkingDaysQuery, GetWorkingDaysQueryVariables>(GetWorkingDaysDocument, options);
      }
export function useGetWorkingDaysLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetWorkingDaysQuery, GetWorkingDaysQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetWorkingDaysQuery, GetWorkingDaysQueryVariables>(GetWorkingDaysDocument, options);
        }
export type GetWorkingDaysQueryHookResult = ReturnType<typeof useGetWorkingDaysQuery>;
export type GetWorkingDaysLazyQueryHookResult = ReturnType<typeof useGetWorkingDaysLazyQuery>;
export type GetWorkingDaysQueryResult = Apollo.QueryResult<GetWorkingDaysQuery, GetWorkingDaysQueryVariables>;
export const GetTeamWorkingDaysDocument = gql`
    query GetTeamWorkingDays {
  getTeamWorkingDays {
    teamCode
    name
    workingHours
    workingDay
    orderDayOfWeek
    active
  }
}
    `;

/**
 * __useGetTeamWorkingDaysQuery__
 *
 * To run a query within a React component, call `useGetTeamWorkingDaysQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamWorkingDaysQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamWorkingDaysQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetTeamWorkingDaysQuery(baseOptions?: Apollo.QueryHookOptions<GetTeamWorkingDaysQuery, GetTeamWorkingDaysQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamWorkingDaysQuery, GetTeamWorkingDaysQueryVariables>(GetTeamWorkingDaysDocument, options);
      }
export function useGetTeamWorkingDaysLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamWorkingDaysQuery, GetTeamWorkingDaysQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamWorkingDaysQuery, GetTeamWorkingDaysQueryVariables>(GetTeamWorkingDaysDocument, options);
        }
export type GetTeamWorkingDaysQueryHookResult = ReturnType<typeof useGetTeamWorkingDaysQuery>;
export type GetTeamWorkingDaysLazyQueryHookResult = ReturnType<typeof useGetTeamWorkingDaysLazyQuery>;
export type GetTeamWorkingDaysQueryResult = Apollo.QueryResult<GetTeamWorkingDaysQuery, GetTeamWorkingDaysQueryVariables>;
export const GetTeamWorkingDaysTeamCodeDocument = gql`
    query GetTeamWorkingDaysTeamCode($teamCode: String!) {
  getTeamWorkingDaysTeamCode(teamCode: $teamCode) {
    teamCode
    name
    workingHours
    workingDay
    orderDayOfWeek
    active
  }
}
    `;

/**
 * __useGetTeamWorkingDaysTeamCodeQuery__
 *
 * To run a query within a React component, call `useGetTeamWorkingDaysTeamCodeQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamWorkingDaysTeamCodeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamWorkingDaysTeamCodeQuery({
 *   variables: {
 *      teamCode: // value for 'teamCode'
 *   },
 * });
 */
export function useGetTeamWorkingDaysTeamCodeQuery(baseOptions: Apollo.QueryHookOptions<GetTeamWorkingDaysTeamCodeQuery, GetTeamWorkingDaysTeamCodeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamWorkingDaysTeamCodeQuery, GetTeamWorkingDaysTeamCodeQueryVariables>(GetTeamWorkingDaysTeamCodeDocument, options);
      }
export function useGetTeamWorkingDaysTeamCodeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamWorkingDaysTeamCodeQuery, GetTeamWorkingDaysTeamCodeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamWorkingDaysTeamCodeQuery, GetTeamWorkingDaysTeamCodeQueryVariables>(GetTeamWorkingDaysTeamCodeDocument, options);
        }
export type GetTeamWorkingDaysTeamCodeQueryHookResult = ReturnType<typeof useGetTeamWorkingDaysTeamCodeQuery>;
export type GetTeamWorkingDaysTeamCodeLazyQueryHookResult = ReturnType<typeof useGetTeamWorkingDaysTeamCodeLazyQuery>;
export type GetTeamWorkingDaysTeamCodeQueryResult = Apollo.QueryResult<GetTeamWorkingDaysTeamCodeQuery, GetTeamWorkingDaysTeamCodeQueryVariables>;
export const GetAvailableTimeDocument = gql`
    query GetAvailableTime($input: AvailableTimeInput!) {
  getAvailableTime(input: $input) {
    ...AvailableTimeOutput
  }
}
    ${AvailableTimeOutputFragmentDoc}
${FirstAvailableTimeFragmentDoc}`;

/**
 * __useGetAvailableTimeQuery__
 *
 * To run a query within a React component, call `useGetAvailableTimeQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAvailableTimeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAvailableTimeQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetAvailableTimeQuery(baseOptions: Apollo.QueryHookOptions<GetAvailableTimeQuery, GetAvailableTimeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAvailableTimeQuery, GetAvailableTimeQueryVariables>(GetAvailableTimeDocument, options);
      }
export function useGetAvailableTimeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAvailableTimeQuery, GetAvailableTimeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAvailableTimeQuery, GetAvailableTimeQueryVariables>(GetAvailableTimeDocument, options);
        }
export type GetAvailableTimeQueryHookResult = ReturnType<typeof useGetAvailableTimeQuery>;
export type GetAvailableTimeLazyQueryHookResult = ReturnType<typeof useGetAvailableTimeLazyQuery>;
export type GetAvailableTimeQueryResult = Apollo.QueryResult<GetAvailableTimeQuery, GetAvailableTimeQueryVariables>;
export const GetTeamAvailableTimeDocument = gql`
    query GetTeamAvailableTime($input: [TeamAvailableTimeInput]!) {
  getTeamAvailableTime(input: $input) {
    ...TeamAvailableTimeOutput
  }
}
    ${TeamAvailableTimeOutputFragmentDoc}
${TeamFirstAvailableTimeFragmentDoc}`;

/**
 * __useGetTeamAvailableTimeQuery__
 *
 * To run a query within a React component, call `useGetTeamAvailableTimeQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamAvailableTimeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamAvailableTimeQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetTeamAvailableTimeQuery(baseOptions: Apollo.QueryHookOptions<GetTeamAvailableTimeQuery, GetTeamAvailableTimeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamAvailableTimeQuery, GetTeamAvailableTimeQueryVariables>(GetTeamAvailableTimeDocument, options);
      }
export function useGetTeamAvailableTimeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamAvailableTimeQuery, GetTeamAvailableTimeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamAvailableTimeQuery, GetTeamAvailableTimeQueryVariables>(GetTeamAvailableTimeDocument, options);
        }
export type GetTeamAvailableTimeQueryHookResult = ReturnType<typeof useGetTeamAvailableTimeQuery>;
export type GetTeamAvailableTimeLazyQueryHookResult = ReturnType<typeof useGetTeamAvailableTimeLazyQuery>;
export type GetTeamAvailableTimeQueryResult = Apollo.QueryResult<GetTeamAvailableTimeQuery, GetTeamAvailableTimeQueryVariables>;
export const GetAppointmentsDocument = gql`
    query GetAppointments($input: AppointmentFilter) {
  getAppointments(input: $input) {
    ...Appointment
  }
}
    ${AppointmentFragmentDoc}
${SelectedServiceFragmentDoc}
${ServiceFragmentDoc}
${ServiceTimePriceFragmentDoc}
${ServiceImagesFragmentDoc}
${ClientFragmentDoc}`;

/**
 * __useGetAppointmentsQuery__
 *
 * To run a query within a React component, call `useGetAppointmentsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAppointmentsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAppointmentsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetAppointmentsQuery(baseOptions?: Apollo.QueryHookOptions<GetAppointmentsQuery, GetAppointmentsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAppointmentsQuery, GetAppointmentsQueryVariables>(GetAppointmentsDocument, options);
      }
export function useGetAppointmentsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAppointmentsQuery, GetAppointmentsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAppointmentsQuery, GetAppointmentsQueryVariables>(GetAppointmentsDocument, options);
        }
export type GetAppointmentsQueryHookResult = ReturnType<typeof useGetAppointmentsQuery>;
export type GetAppointmentsLazyQueryHookResult = ReturnType<typeof useGetAppointmentsLazyQuery>;
export type GetAppointmentsQueryResult = Apollo.QueryResult<GetAppointmentsQuery, GetAppointmentsQueryVariables>;
export const GetTeamAppointmentsDocument = gql`
    query GetTeamAppointments($input: TeamAppointmentFilter) {
  getTeamAppointments(input: $input) {
    ...TeamAppointment
  }
}
    ${TeamAppointmentFragmentDoc}
${SelectedServiceFragmentDoc}
${ServiceFragmentDoc}
${ServiceTimePriceFragmentDoc}
${ServiceImagesFragmentDoc}
${ClientFragmentDoc}`;

/**
 * __useGetTeamAppointmentsQuery__
 *
 * To run a query within a React component, call `useGetTeamAppointmentsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamAppointmentsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamAppointmentsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetTeamAppointmentsQuery(baseOptions?: Apollo.QueryHookOptions<GetTeamAppointmentsQuery, GetTeamAppointmentsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamAppointmentsQuery, GetTeamAppointmentsQueryVariables>(GetTeamAppointmentsDocument, options);
      }
export function useGetTeamAppointmentsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamAppointmentsQuery, GetTeamAppointmentsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamAppointmentsQuery, GetTeamAppointmentsQueryVariables>(GetTeamAppointmentsDocument, options);
        }
export type GetTeamAppointmentsQueryHookResult = ReturnType<typeof useGetTeamAppointmentsQuery>;
export type GetTeamAppointmentsLazyQueryHookResult = ReturnType<typeof useGetTeamAppointmentsLazyQuery>;
export type GetTeamAppointmentsQueryResult = Apollo.QueryResult<GetTeamAppointmentsQuery, GetTeamAppointmentsQueryVariables>;
export const AddAppointmentDocument = gql`
    mutation AddAppointment($input: AppointmentInput) {
  addAppointment(input: $input) {
    ...AppointmentOutput
  }
}
    ${AppointmentOutputFragmentDoc}`;
export type AddAppointmentMutationFn = Apollo.MutationFunction<AddAppointmentMutation, AddAppointmentMutationVariables>;

/**
 * __useAddAppointmentMutation__
 *
 * To run a mutation, you first call `useAddAppointmentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddAppointmentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addAppointmentMutation, { data, loading, error }] = useAddAppointmentMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddAppointmentMutation(baseOptions?: Apollo.MutationHookOptions<AddAppointmentMutation, AddAppointmentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddAppointmentMutation, AddAppointmentMutationVariables>(AddAppointmentDocument, options);
      }
export type AddAppointmentMutationHookResult = ReturnType<typeof useAddAppointmentMutation>;
export type AddAppointmentMutationResult = Apollo.MutationResult<AddAppointmentMutation>;
export type AddAppointmentMutationOptions = Apollo.BaseMutationOptions<AddAppointmentMutation, AddAppointmentMutationVariables>;
export const AddTeamAppointmentDocument = gql`
    mutation AddTeamAppointment($input: TeamAppointmentInput) {
  addTeamAppointment(input: $input) {
    ...TeamAppointmentOutput
  }
}
    ${TeamAppointmentOutputFragmentDoc}`;
export type AddTeamAppointmentMutationFn = Apollo.MutationFunction<AddTeamAppointmentMutation, AddTeamAppointmentMutationVariables>;

/**
 * __useAddTeamAppointmentMutation__
 *
 * To run a mutation, you first call `useAddTeamAppointmentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddTeamAppointmentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addTeamAppointmentMutation, { data, loading, error }] = useAddTeamAppointmentMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddTeamAppointmentMutation(baseOptions?: Apollo.MutationHookOptions<AddTeamAppointmentMutation, AddTeamAppointmentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddTeamAppointmentMutation, AddTeamAppointmentMutationVariables>(AddTeamAppointmentDocument, options);
      }
export type AddTeamAppointmentMutationHookResult = ReturnType<typeof useAddTeamAppointmentMutation>;
export type AddTeamAppointmentMutationResult = Apollo.MutationResult<AddTeamAppointmentMutation>;
export type AddTeamAppointmentMutationOptions = Apollo.BaseMutationOptions<AddTeamAppointmentMutation, AddTeamAppointmentMutationVariables>;
export const UpdateGooglePlaceDocument = gql`
    mutation UpdateGooglePlace($input: GooglePlaceInput!) {
  updateGooglePlace(input: $input) {
    ...GooglePlace
  }
}
    ${GooglePlaceFragmentDoc}
${GoogleReviewFragmentDoc}`;
export type UpdateGooglePlaceMutationFn = Apollo.MutationFunction<UpdateGooglePlaceMutation, UpdateGooglePlaceMutationVariables>;

/**
 * __useUpdateGooglePlaceMutation__
 *
 * To run a mutation, you first call `useUpdateGooglePlaceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateGooglePlaceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateGooglePlaceMutation, { data, loading, error }] = useUpdateGooglePlaceMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateGooglePlaceMutation(baseOptions?: Apollo.MutationHookOptions<UpdateGooglePlaceMutation, UpdateGooglePlaceMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateGooglePlaceMutation, UpdateGooglePlaceMutationVariables>(UpdateGooglePlaceDocument, options);
      }
export type UpdateGooglePlaceMutationHookResult = ReturnType<typeof useUpdateGooglePlaceMutation>;
export type UpdateGooglePlaceMutationResult = Apollo.MutationResult<UpdateGooglePlaceMutation>;
export type UpdateGooglePlaceMutationOptions = Apollo.BaseMutationOptions<UpdateGooglePlaceMutation, UpdateGooglePlaceMutationVariables>;
export const GetGooglePlaceDocument = gql`
    query GetGooglePlace($id: String!) {
  getGooglePlace(id: $id) {
    ...GooglePlace
  }
}
    ${GooglePlaceFragmentDoc}
${GoogleReviewFragmentDoc}`;

/**
 * __useGetGooglePlaceQuery__
 *
 * To run a query within a React component, call `useGetGooglePlaceQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetGooglePlaceQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetGooglePlaceQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetGooglePlaceQuery(baseOptions: Apollo.QueryHookOptions<GetGooglePlaceQuery, GetGooglePlaceQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetGooglePlaceQuery, GetGooglePlaceQueryVariables>(GetGooglePlaceDocument, options);
      }
export function useGetGooglePlaceLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetGooglePlaceQuery, GetGooglePlaceQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetGooglePlaceQuery, GetGooglePlaceQueryVariables>(GetGooglePlaceDocument, options);
        }
export type GetGooglePlaceQueryHookResult = ReturnType<typeof useGetGooglePlaceQuery>;
export type GetGooglePlaceLazyQueryHookResult = ReturnType<typeof useGetGooglePlaceLazyQuery>;
export type GetGooglePlaceQueryResult = Apollo.QueryResult<GetGooglePlaceQuery, GetGooglePlaceQueryVariables>;
export const AddServiceDocument = gql`
    mutation AddService($input: ServiceInput!) {
  addService(input: $input) {
    ...Service
  }
}
    ${ServiceFragmentDoc}
${ServiceTimePriceFragmentDoc}
${ServiceImagesFragmentDoc}`;
export type AddServiceMutationFn = Apollo.MutationFunction<AddServiceMutation, AddServiceMutationVariables>;

/**
 * __useAddServiceMutation__
 *
 * To run a mutation, you first call `useAddServiceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddServiceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addServiceMutation, { data, loading, error }] = useAddServiceMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddServiceMutation(baseOptions?: Apollo.MutationHookOptions<AddServiceMutation, AddServiceMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddServiceMutation, AddServiceMutationVariables>(AddServiceDocument, options);
      }
export type AddServiceMutationHookResult = ReturnType<typeof useAddServiceMutation>;
export type AddServiceMutationResult = Apollo.MutationResult<AddServiceMutation>;
export type AddServiceMutationOptions = Apollo.BaseMutationOptions<AddServiceMutation, AddServiceMutationVariables>;
export const ConfirmCancelAppointmentDocument = gql`
    mutation ConfirmCancelAppointment($id: ID!, $appintmentStatus: AppointmentStatus!) {
  confirmCancelAppointment(id: $id, appintmentStatus: $appintmentStatus) {
    ...AppointmentOutput
  }
}
    ${AppointmentOutputFragmentDoc}`;
export type ConfirmCancelAppointmentMutationFn = Apollo.MutationFunction<ConfirmCancelAppointmentMutation, ConfirmCancelAppointmentMutationVariables>;

/**
 * __useConfirmCancelAppointmentMutation__
 *
 * To run a mutation, you first call `useConfirmCancelAppointmentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useConfirmCancelAppointmentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [confirmCancelAppointmentMutation, { data, loading, error }] = useConfirmCancelAppointmentMutation({
 *   variables: {
 *      id: // value for 'id'
 *      appintmentStatus: // value for 'appintmentStatus'
 *   },
 * });
 */
export function useConfirmCancelAppointmentMutation(baseOptions?: Apollo.MutationHookOptions<ConfirmCancelAppointmentMutation, ConfirmCancelAppointmentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ConfirmCancelAppointmentMutation, ConfirmCancelAppointmentMutationVariables>(ConfirmCancelAppointmentDocument, options);
      }
export type ConfirmCancelAppointmentMutationHookResult = ReturnType<typeof useConfirmCancelAppointmentMutation>;
export type ConfirmCancelAppointmentMutationResult = Apollo.MutationResult<ConfirmCancelAppointmentMutation>;
export type ConfirmCancelAppointmentMutationOptions = Apollo.BaseMutationOptions<ConfirmCancelAppointmentMutation, ConfirmCancelAppointmentMutationVariables>;
export const ConfirmCancelTeamAppointmentDocument = gql`
    mutation ConfirmCancelTeamAppointment($id: ID!, $appintmentStatus: AppointmentStatus!) {
  confirmCancelTeamAppointment(id: $id, appintmentStatus: $appintmentStatus) {
    ...TeamAppointmentOutput
  }
}
    ${TeamAppointmentOutputFragmentDoc}`;
export type ConfirmCancelTeamAppointmentMutationFn = Apollo.MutationFunction<ConfirmCancelTeamAppointmentMutation, ConfirmCancelTeamAppointmentMutationVariables>;

/**
 * __useConfirmCancelTeamAppointmentMutation__
 *
 * To run a mutation, you first call `useConfirmCancelTeamAppointmentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useConfirmCancelTeamAppointmentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [confirmCancelTeamAppointmentMutation, { data, loading, error }] = useConfirmCancelTeamAppointmentMutation({
 *   variables: {
 *      id: // value for 'id'
 *      appintmentStatus: // value for 'appintmentStatus'
 *   },
 * });
 */
export function useConfirmCancelTeamAppointmentMutation(baseOptions?: Apollo.MutationHookOptions<ConfirmCancelTeamAppointmentMutation, ConfirmCancelTeamAppointmentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ConfirmCancelTeamAppointmentMutation, ConfirmCancelTeamAppointmentMutationVariables>(ConfirmCancelTeamAppointmentDocument, options);
      }
export type ConfirmCancelTeamAppointmentMutationHookResult = ReturnType<typeof useConfirmCancelTeamAppointmentMutation>;
export type ConfirmCancelTeamAppointmentMutationResult = Apollo.MutationResult<ConfirmCancelTeamAppointmentMutation>;
export type ConfirmCancelTeamAppointmentMutationOptions = Apollo.BaseMutationOptions<ConfirmCancelTeamAppointmentMutation, ConfirmCancelTeamAppointmentMutationVariables>;
export const AddClientReviewDocument = gql`
    mutation AddClientReview($mobile: ID!, $date: String!, $isGiven: Boolean!) {
  addClientReview(mobile: $mobile, date: $date, isGiven: $isGiven) {
    ...Client
  }
}
    ${ClientFragmentDoc}`;
export type AddClientReviewMutationFn = Apollo.MutationFunction<AddClientReviewMutation, AddClientReviewMutationVariables>;

/**
 * __useAddClientReviewMutation__
 *
 * To run a mutation, you first call `useAddClientReviewMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddClientReviewMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addClientReviewMutation, { data, loading, error }] = useAddClientReviewMutation({
 *   variables: {
 *      mobile: // value for 'mobile'
 *      date: // value for 'date'
 *      isGiven: // value for 'isGiven'
 *   },
 * });
 */
export function useAddClientReviewMutation(baseOptions?: Apollo.MutationHookOptions<AddClientReviewMutation, AddClientReviewMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddClientReviewMutation, AddClientReviewMutationVariables>(AddClientReviewDocument, options);
      }
export type AddClientReviewMutationHookResult = ReturnType<typeof useAddClientReviewMutation>;
export type AddClientReviewMutationResult = Apollo.MutationResult<AddClientReviewMutation>;
export type AddClientReviewMutationOptions = Apollo.BaseMutationOptions<AddClientReviewMutation, AddClientReviewMutationVariables>;
export const AddBlockTimesDocument = gql`
    mutation AddBlockTimes($input: [BlockTimeInput]!) {
  addBlockTimes(input: $input) {
    ...BlockTime
  }
}
    ${BlockTimeFragmentDoc}`;
export type AddBlockTimesMutationFn = Apollo.MutationFunction<AddBlockTimesMutation, AddBlockTimesMutationVariables>;

/**
 * __useAddBlockTimesMutation__
 *
 * To run a mutation, you first call `useAddBlockTimesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddBlockTimesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addBlockTimesMutation, { data, loading, error }] = useAddBlockTimesMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddBlockTimesMutation(baseOptions?: Apollo.MutationHookOptions<AddBlockTimesMutation, AddBlockTimesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddBlockTimesMutation, AddBlockTimesMutationVariables>(AddBlockTimesDocument, options);
      }
export type AddBlockTimesMutationHookResult = ReturnType<typeof useAddBlockTimesMutation>;
export type AddBlockTimesMutationResult = Apollo.MutationResult<AddBlockTimesMutation>;
export type AddBlockTimesMutationOptions = Apollo.BaseMutationOptions<AddBlockTimesMutation, AddBlockTimesMutationVariables>;
export const AddTeamBlockTimesDocument = gql`
    mutation AddTeamBlockTimes($input: [TeamBlockTimeInput]!) {
  addTeamBlockTimes(input: $input) {
    ...TeamBlockTime
  }
}
    ${TeamBlockTimeFragmentDoc}`;
export type AddTeamBlockTimesMutationFn = Apollo.MutationFunction<AddTeamBlockTimesMutation, AddTeamBlockTimesMutationVariables>;

/**
 * __useAddTeamBlockTimesMutation__
 *
 * To run a mutation, you first call `useAddTeamBlockTimesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddTeamBlockTimesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addTeamBlockTimesMutation, { data, loading, error }] = useAddTeamBlockTimesMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddTeamBlockTimesMutation(baseOptions?: Apollo.MutationHookOptions<AddTeamBlockTimesMutation, AddTeamBlockTimesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddTeamBlockTimesMutation, AddTeamBlockTimesMutationVariables>(AddTeamBlockTimesDocument, options);
      }
export type AddTeamBlockTimesMutationHookResult = ReturnType<typeof useAddTeamBlockTimesMutation>;
export type AddTeamBlockTimesMutationResult = Apollo.MutationResult<AddTeamBlockTimesMutation>;
export type AddTeamBlockTimesMutationOptions = Apollo.BaseMutationOptions<AddTeamBlockTimesMutation, AddTeamBlockTimesMutationVariables>;
export const AddClientDocument = gql`
    mutation AddClient($input: ClientInput!) {
  addClient(input: $input) {
    ...Client
  }
}
    ${ClientFragmentDoc}`;
export type AddClientMutationFn = Apollo.MutationFunction<AddClientMutation, AddClientMutationVariables>;

/**
 * __useAddClientMutation__
 *
 * To run a mutation, you first call `useAddClientMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddClientMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addClientMutation, { data, loading, error }] = useAddClientMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddClientMutation(baseOptions?: Apollo.MutationHookOptions<AddClientMutation, AddClientMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddClientMutation, AddClientMutationVariables>(AddClientDocument, options);
      }
export type AddClientMutationHookResult = ReturnType<typeof useAddClientMutation>;
export type AddClientMutationResult = Apollo.MutationResult<AddClientMutation>;
export type AddClientMutationOptions = Apollo.BaseMutationOptions<AddClientMutation, AddClientMutationVariables>;
export const GetBlockTimesDocument = gql`
    query GetBlockTimes {
  getBlockTimes {
    ...BlockTime
  }
}
    ${BlockTimeFragmentDoc}`;

/**
 * __useGetBlockTimesQuery__
 *
 * To run a query within a React component, call `useGetBlockTimesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetBlockTimesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetBlockTimesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetBlockTimesQuery(baseOptions?: Apollo.QueryHookOptions<GetBlockTimesQuery, GetBlockTimesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetBlockTimesQuery, GetBlockTimesQueryVariables>(GetBlockTimesDocument, options);
      }
export function useGetBlockTimesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetBlockTimesQuery, GetBlockTimesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetBlockTimesQuery, GetBlockTimesQueryVariables>(GetBlockTimesDocument, options);
        }
export type GetBlockTimesQueryHookResult = ReturnType<typeof useGetBlockTimesQuery>;
export type GetBlockTimesLazyQueryHookResult = ReturnType<typeof useGetBlockTimesLazyQuery>;
export type GetBlockTimesQueryResult = Apollo.QueryResult<GetBlockTimesQuery, GetBlockTimesQueryVariables>;
export const GetTeamBlockTimesDocument = gql`
    query GetTeamBlockTimes($teamCode: String!) {
  getTeamBlockTimes(teamCode: $teamCode) {
    ...TeamBlockTime
  }
}
    ${TeamBlockTimeFragmentDoc}`;

/**
 * __useGetTeamBlockTimesQuery__
 *
 * To run a query within a React component, call `useGetTeamBlockTimesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamBlockTimesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamBlockTimesQuery({
 *   variables: {
 *      teamCode: // value for 'teamCode'
 *   },
 * });
 */
export function useGetTeamBlockTimesQuery(baseOptions: Apollo.QueryHookOptions<GetTeamBlockTimesQuery, GetTeamBlockTimesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamBlockTimesQuery, GetTeamBlockTimesQueryVariables>(GetTeamBlockTimesDocument, options);
      }
export function useGetTeamBlockTimesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamBlockTimesQuery, GetTeamBlockTimesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamBlockTimesQuery, GetTeamBlockTimesQueryVariables>(GetTeamBlockTimesDocument, options);
        }
export type GetTeamBlockTimesQueryHookResult = ReturnType<typeof useGetTeamBlockTimesQuery>;
export type GetTeamBlockTimesLazyQueryHookResult = ReturnType<typeof useGetTeamBlockTimesLazyQuery>;
export type GetTeamBlockTimesQueryResult = Apollo.QueryResult<GetTeamBlockTimesQuery, GetTeamBlockTimesQueryVariables>;
export const GetTeamBlockTimesAllDocument = gql`
    query GetTeamBlockTimesAll {
  getTeamBlockTimesAll {
    ...TeamBlockTime
  }
}
    ${TeamBlockTimeFragmentDoc}`;

/**
 * __useGetTeamBlockTimesAllQuery__
 *
 * To run a query within a React component, call `useGetTeamBlockTimesAllQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamBlockTimesAllQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamBlockTimesAllQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetTeamBlockTimesAllQuery(baseOptions?: Apollo.QueryHookOptions<GetTeamBlockTimesAllQuery, GetTeamBlockTimesAllQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamBlockTimesAllQuery, GetTeamBlockTimesAllQueryVariables>(GetTeamBlockTimesAllDocument, options);
      }
export function useGetTeamBlockTimesAllLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamBlockTimesAllQuery, GetTeamBlockTimesAllQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamBlockTimesAllQuery, GetTeamBlockTimesAllQueryVariables>(GetTeamBlockTimesAllDocument, options);
        }
export type GetTeamBlockTimesAllQueryHookResult = ReturnType<typeof useGetTeamBlockTimesAllQuery>;
export type GetTeamBlockTimesAllLazyQueryHookResult = ReturnType<typeof useGetTeamBlockTimesAllLazyQuery>;
export type GetTeamBlockTimesAllQueryResult = Apollo.QueryResult<GetTeamBlockTimesAllQuery, GetTeamBlockTimesAllQueryVariables>;
export const DeleteBlockTimesDocument = gql`
    mutation DeleteBlockTimes($ids: [ID!]) {
  deleteBlockTimes(ids: $ids) {
    id
  }
}
    `;
export type DeleteBlockTimesMutationFn = Apollo.MutationFunction<DeleteBlockTimesMutation, DeleteBlockTimesMutationVariables>;

/**
 * __useDeleteBlockTimesMutation__
 *
 * To run a mutation, you first call `useDeleteBlockTimesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteBlockTimesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteBlockTimesMutation, { data, loading, error }] = useDeleteBlockTimesMutation({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useDeleteBlockTimesMutation(baseOptions?: Apollo.MutationHookOptions<DeleteBlockTimesMutation, DeleteBlockTimesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteBlockTimesMutation, DeleteBlockTimesMutationVariables>(DeleteBlockTimesDocument, options);
      }
export type DeleteBlockTimesMutationHookResult = ReturnType<typeof useDeleteBlockTimesMutation>;
export type DeleteBlockTimesMutationResult = Apollo.MutationResult<DeleteBlockTimesMutation>;
export type DeleteBlockTimesMutationOptions = Apollo.BaseMutationOptions<DeleteBlockTimesMutation, DeleteBlockTimesMutationVariables>;
export const DeleteTeamBlockTimesDocument = gql`
    mutation DeleteTeamBlockTimes($ids: [ID!]) {
  deleteTeamBlockTimes(ids: $ids) {
    id
  }
}
    `;
export type DeleteTeamBlockTimesMutationFn = Apollo.MutationFunction<DeleteTeamBlockTimesMutation, DeleteTeamBlockTimesMutationVariables>;

/**
 * __useDeleteTeamBlockTimesMutation__
 *
 * To run a mutation, you first call `useDeleteTeamBlockTimesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteTeamBlockTimesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteTeamBlockTimesMutation, { data, loading, error }] = useDeleteTeamBlockTimesMutation({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useDeleteTeamBlockTimesMutation(baseOptions?: Apollo.MutationHookOptions<DeleteTeamBlockTimesMutation, DeleteTeamBlockTimesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteTeamBlockTimesMutation, DeleteTeamBlockTimesMutationVariables>(DeleteTeamBlockTimesDocument, options);
      }
export type DeleteTeamBlockTimesMutationHookResult = ReturnType<typeof useDeleteTeamBlockTimesMutation>;
export type DeleteTeamBlockTimesMutationResult = Apollo.MutationResult<DeleteTeamBlockTimesMutation>;
export type DeleteTeamBlockTimesMutationOptions = Apollo.BaseMutationOptions<DeleteTeamBlockTimesMutation, DeleteTeamBlockTimesMutationVariables>;
export const UpdateClientForReviewDocument = gql`
    mutation UpdateClientForReview($mobile: ID!) {
  updateClientForReview(mobile: $mobile) {
    ...Client
  }
}
    ${ClientFragmentDoc}`;
export type UpdateClientForReviewMutationFn = Apollo.MutationFunction<UpdateClientForReviewMutation, UpdateClientForReviewMutationVariables>;

/**
 * __useUpdateClientForReviewMutation__
 *
 * To run a mutation, you first call `useUpdateClientForReviewMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateClientForReviewMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateClientForReviewMutation, { data, loading, error }] = useUpdateClientForReviewMutation({
 *   variables: {
 *      mobile: // value for 'mobile'
 *   },
 * });
 */
export function useUpdateClientForReviewMutation(baseOptions?: Apollo.MutationHookOptions<UpdateClientForReviewMutation, UpdateClientForReviewMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateClientForReviewMutation, UpdateClientForReviewMutationVariables>(UpdateClientForReviewDocument, options);
      }
export type UpdateClientForReviewMutationHookResult = ReturnType<typeof useUpdateClientForReviewMutation>;
export type UpdateClientForReviewMutationResult = Apollo.MutationResult<UpdateClientForReviewMutation>;
export type UpdateClientForReviewMutationOptions = Apollo.BaseMutationOptions<UpdateClientForReviewMutation, UpdateClientForReviewMutationVariables>;
export const UpdateClientForFollowDocument = gql`
    mutation UpdateClientForFollow($mobile: ID!) {
  updateClientForFollow(mobile: $mobile) {
    ...Client
  }
}
    ${ClientFragmentDoc}`;
export type UpdateClientForFollowMutationFn = Apollo.MutationFunction<UpdateClientForFollowMutation, UpdateClientForFollowMutationVariables>;

/**
 * __useUpdateClientForFollowMutation__
 *
 * To run a mutation, you first call `useUpdateClientForFollowMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateClientForFollowMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateClientForFollowMutation, { data, loading, error }] = useUpdateClientForFollowMutation({
 *   variables: {
 *      mobile: // value for 'mobile'
 *   },
 * });
 */
export function useUpdateClientForFollowMutation(baseOptions?: Apollo.MutationHookOptions<UpdateClientForFollowMutation, UpdateClientForFollowMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateClientForFollowMutation, UpdateClientForFollowMutationVariables>(UpdateClientForFollowDocument, options);
      }
export type UpdateClientForFollowMutationHookResult = ReturnType<typeof useUpdateClientForFollowMutation>;
export type UpdateClientForFollowMutationResult = Apollo.MutationResult<UpdateClientForFollowMutation>;
export type UpdateClientForFollowMutationOptions = Apollo.BaseMutationOptions<UpdateClientForFollowMutation, UpdateClientForFollowMutationVariables>;
export const AuthenticateDocument = gql`
    query Authenticate($input: AuthenticationInput) {
  authenticate(input: $input) {
    ...AuthenticationOutput
  }
}
    ${AuthenticationOutputFragmentDoc}`;

/**
 * __useAuthenticateQuery__
 *
 * To run a query within a React component, call `useAuthenticateQuery` and pass it any options that fit your needs.
 * When your component renders, `useAuthenticateQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAuthenticateQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAuthenticateQuery(baseOptions?: Apollo.QueryHookOptions<AuthenticateQuery, AuthenticateQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<AuthenticateQuery, AuthenticateQueryVariables>(AuthenticateDocument, options);
      }
export function useAuthenticateLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<AuthenticateQuery, AuthenticateQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<AuthenticateQuery, AuthenticateQueryVariables>(AuthenticateDocument, options);
        }
export type AuthenticateQueryHookResult = ReturnType<typeof useAuthenticateQuery>;
export type AuthenticateLazyQueryHookResult = ReturnType<typeof useAuthenticateLazyQuery>;
export type AuthenticateQueryResult = Apollo.QueryResult<AuthenticateQuery, AuthenticateQueryVariables>;