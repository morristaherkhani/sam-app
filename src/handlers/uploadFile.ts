import { APIGatewayProxyEvent } from 'aws-lambda';
import AWS, { AWSError } from 'aws-sdk';
import parser from 'lambda-multipart-parser';
import path from 'path';
import { putImage } from '../dynamodb/putImage';
import { S3_BUCKET_NAME, getUrlFromBucket } from '../utils';

const s3 = new AWS.S3();
export const handler = async (event: APIGatewayProxyEvent) => {
    try {
        const { description, files, s3Path } = await parser.parse(event);
        const result = await Promise.all(
            files.map(async (file) => {
                const filenameId = `${Date.now()}${Math.round(Math.random() * 1e9)}`;
                const filename = `${s3Path}/${filenameId}${path.extname(file.filename)}`;
                await s3
                    .putObject({
                        Bucket: S3_BUCKET_NAME,
                        Key: filename,
                        ACL: 'public-read',
                        Body: file.content,
                    })
                    .promise();
                const image = {
                    description,
                    id: filenameId,
                    path: s3Path,
                    url: getUrlFromBucket(filename),
                };
                await putImage(image);
                return image;
            }),
        );
        const response = {
            statusCode: 200,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({ result }),
        };
        return response;
    } catch (error) {
        throw error as AWSError;
    }
};
