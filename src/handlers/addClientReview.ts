import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { ClientsForReviewFollow, MutationAddClientReviewArgs } from '../../generated/graphql';
import { AWSError } from 'aws-sdk';
import { putClientReview } from '../dynamodb/putClientReview';

export const handler: AppSyncResolverHandler<MutationAddClientReviewArgs, ClientsForReviewFollow | undefined> = async (
    event: AppSyncResolverEvent<MutationAddClientReviewArgs>,
): Promise<ClientsForReviewFollow | undefined> => {
    if (!event || !event.arguments || !event.arguments.mobile || !event.arguments.date) {
        return undefined;
    }
    const { mobile, date, isGiven } = event.arguments;
    try {
        const result = await putClientReview(mobile, date, isGiven);
        return result;
    } catch (error) {
        throw error as AWSError;
    }
};
