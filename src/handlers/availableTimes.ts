import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { addMinutes, addDays, format } from 'date-fns';
import { isArray } from 'lodash';
import {
    AvailableTimeOutput,
    FirstAvailableTime,
    QueryGetAvailableTimeArgs,
    ServiceTimePrice,
} from '../../generated/graphql';
import { getBlockTimes } from '../dynamodb/getBlockTimes';
import { getAppointmentsByDate } from '../dynamodb/getAppointments';
import { getServices } from '../dynamodb/getServices';
import { IAppointmentsTable, IBlockTimeTable, IServiceTable, IServiceTimePrice } from '../dynamodb/types';
import { AWSError } from 'aws-sdk';
import { getWorkingDay } from '../dynamodb/getWorkingDay';
const TIME_PERIOD = 15;
type NotAvailableTimeSlots = Array<{ notAvailableTimeStart: Date; notAvailableTimeEnd: Date }>;

const todayDate = new Date(new Date().toLocaleString('en', { timeZone: 'Australia/Melbourne' }));
const today = format(todayDate, 'yyyy-MM-dd');

export const handler: AppSyncResolverHandler<
    QueryGetAvailableTimeArgs,
    Pick<AvailableTimeOutput, 'avaliableTimes' | 'firstAvailableTime'> | undefined
> = async (
    event: AppSyncResolverEvent<QueryGetAvailableTimeArgs>,
): Promise<Pick<AvailableTimeOutput, 'avaliableTimes' | 'firstAvailableTime'> | undefined> => {
    console.log('event.arguments:', event.arguments);
    if (
        !event.arguments.input?.date ||
        !event.arguments.input?.serviceDuration ||
        !event.arguments.input?.serviceMargin
    ) {
        // appDate is not passed
        return {
            avaliableTimes: [],
        };
    }
    const avaliableTimes = await getAvailableTime(
        event.arguments.input.date,
        event.arguments.input.serviceDuration,
        event.arguments.input.serviceMargin,
    );
    return {
        avaliableTimes: isArray(avaliableTimes) ? avaliableTimes : [],
        firstAvailableTime:
            isArray(avaliableTimes) && Boolean(avaliableTimes?.length)
                ? undefined
                : ((await getAvailableTime(
                      event.arguments.input.date,
                      event.arguments.input.serviceDuration,
                      event.arguments.input.serviceMargin,
                      [],
                  )) as FirstAvailableTime),
    };
};

const getAvailableTime = async (
    inputDateParam: string,
    inputserviceDuration: number,
    inputserviceMargin: number,
    firstAvailableTime?: string[],
): Promise<string[] | undefined | FirstAvailableTime> => {
    const lookingForFirstAvailable = Boolean(firstAvailableTime);
    console.log('lookingForFirstAvailable:', lookingForFirstAvailable);

    if (lookingForFirstAvailable && firstAvailableTime && Boolean(firstAvailableTime.length)) {
        console.log('firstAvailableTime is found:', firstAvailableTime);
        return { avaliableTimes: firstAvailableTime, date: inputDateParam };
    }

    let inputDate = inputDateParam;
    if (lookingForFirstAvailable) {
        inputDate = format(addDays(new Date(inputDateParam), 1), 'yyyy-MM-dd');
    }
    console.log('inputDate:', inputDate);

    try {
        const workingDay = await getWorkingDay(inputDate);

        if (!(workingDay && workingDay.workingDay)) {
            // not a working day
            return !lookingForFirstAvailable
                ? []
                : getAvailableTime(inputDate, inputserviceDuration, inputserviceMargin, firstAvailableTime);
        }

        const blockTimes = await getBlockTimes(inputDate);
        if (blockTimes?.isBlockedWholeDay) {
            return !lookingForFirstAvailable
                ? []
                : getAvailableTime(inputDate, inputserviceDuration, inputserviceMargin, firstAvailableTime);
        }

        const notAvailableSlots: NotAvailableTimeSlots = await getNotAvailableSlots(inputDate, blockTimes);
        console.log(
            'notAvailableSlots:',
            notAvailableSlots.map(({ notAvailableTimeEnd, notAvailableTimeStart }) => ({
                notAvailableTimeStart: format(notAvailableTimeStart, 'HH:mm'),
                notAvailableTimeEnd: format(notAvailableTimeEnd, 'HH:mm'),
            })),
        );
        let workingTimes: Array<string> = [];

        for (const workingHour of workingDay.workingHours) {
            if (!workingHour) {
                return !lookingForFirstAvailable
                    ? []
                    : getAvailableTime(inputDate, inputserviceDuration, inputserviceMargin, firstAvailableTime);
            }
            // workingHour: 15:00-20:00
            console.log('workingHour:', workingHour);
            // eslint-disable-next-line prefer-const,
            let [startWorkingHours, endWorkingHours] = workingHour
                .split('-')
                .map((slot) => new Date(`${today}T` + slot));

            if (inputDate === today) {
                console.log('inputDate is today', todayDate);
                while (startWorkingHours < todayDate) {
                    console.log('startWorkingHours in the loop', startWorkingHours);

                    startWorkingHours = addMinutes(startWorkingHours, 15);
                }
            }
            console.log('startWorkingHours:', format(startWorkingHours, 'HH:mm'));
            console.log('endWorkingHours:', format(endWorkingHours, 'HH:mm'));
            if (!startWorkingHours || !endWorkingHours) {
                return !lookingForFirstAvailable
                    ? []
                    : getAvailableTime(inputDate, inputserviceDuration, inputserviceMargin, firstAvailableTime);
            }
            endWorkingHours = addMinutes(endWorkingHours, inputserviceMargin); //adding margin to the end of the slot

            workingTimes = [
                ...workingTimes,
                ...getWorkingHoursForWorkingHoursSlot(
                    startWorkingHours,
                    endWorkingHours,
                    inputserviceDuration,
                    inputserviceMargin,
                    notAvailableSlots,
                ),
            ];
        }
        return !lookingForFirstAvailable
            ? workingTimes
            : getAvailableTime(inputDate, inputserviceDuration, inputserviceMargin, workingTimes);
    } catch (error) {
        console.log('error:', error);
        throw error as AWSError;
    }
};

const checkForConflict = (appS: Date, appE: Date, serS: Date, serE: Date) => {
    if (
        (serE > appS && serS < appS) ||
        (serS === appS && serE === appE) ||
        (serS >= appS && serE < appE) ||
        (serS >= appS && serE <= appE) ||
        (serS < appE && serE > appE)
    ) {
        return true;
    }
    return false;
};

const getNotAvailableTimeSlotsAppointments = (
    appointments: IAppointmentsTable[] | undefined,
    services: IServiceTable[] | undefined,
) => {
    if (Boolean(appointments?.length) && Boolean(services?.length)) {
        return appointments?.map((appointment) => {
            const notAvailableTimeStart = new Date(`${today}T` + appointment.appTime);
            const serviceTimePrice: IServiceTimePrice | undefined = services
                ?.find(
                    (service) => service.code === (appointment.selectedService as { serviceCode: string }).serviceCode,
                )
                ?.timePrice.find(
                    (t: ServiceTimePrice) =>
                        t.code ===
                        (appointment.selectedService as { serviceTimePriceCode: string }).serviceTimePriceCode,
                );
            const notAvailableTimeEnd = addMinutes(
                new Date(`${today}T` + appointment.appTime),
                (serviceTimePrice?.duration ?? 0) + (serviceTimePrice?.margin ?? 0),
            );
            return {
                notAvailableTimeStart,
                notAvailableTimeEnd,
            };
        });
    }

    return [];
};

const getNotAvailableTimeSlotsBlockTime = (blockTime: IBlockTimeTable[]) => {
    if (Boolean(blockTime?.length)) {
        return blockTime?.map((blockTime) => ({
            notAvailableTimeStart: new Date(`${today}T` + blockTime.time.split('-')[0]),
            notAvailableTimeEnd: new Date(`${today}T` + blockTime.time.split('-')[1]),
        }));
    }
    return [];
};

const getNotAvailableSlots = async (
    inputDate: string,
    blockTimes:
        | {
              isBlockedWholeDay: boolean;
              blockTime: IBlockTimeTable[];
          }
        | undefined,
) => {
    const appointments = await getAppointmentsByDate(inputDate);
    const services = await getServices(['timePrice']);
    console.log('services:', services);
    return [
        ...getNotAvailableTimeSlotsBlockTime(blockTimes?.blockTime ?? []),
        ...(getNotAvailableTimeSlotsAppointments(appointments, services) ?? []),
    ];
};
type Conflict = {
    notAvailableTimeStart: Date;
    notAvailableTimeEnd: Date;
};
const getConflicts = (
    notAvailableSlots: NotAvailableTimeSlots,
    serviceStartTime: Date,
    serviceEndTime: Date,
): Conflict | undefined => {
    if (Boolean(notAvailableSlots.length)) {
        return notAvailableSlots?.find((slot) =>
            checkForConflict(slot.notAvailableTimeStart, slot.notAvailableTimeEnd, serviceStartTime, serviceEndTime),
        );
    }
};
const getWorkingHoursForWorkingHoursSlot = (
    startWorkingHours: Date,
    endWorkingHours: Date,
    inputserviceDuration: number,
    inputserviceMargin: number,
    notAvailableSlots: NotAvailableTimeSlots,
): Array<string> => {
    const workingTimes: Array<string> = [];
    let serviceStartTime = new Date(startWorkingHours);
    let serviceEndTime = addMinutes(
        new Date(startWorkingHours),
        Number(inputserviceDuration) + Number(inputserviceMargin),
    );
    console.log('serviceStartTime:', format(serviceStartTime, 'HH:mm'));
    console.log('serviceEndTime:', format(serviceEndTime, 'HH:mm'));
    while (serviceEndTime <= endWorkingHours) {
        console.log('serviceStartTime:', `${serviceStartTime.getHours()}:${serviceStartTime.getMinutes()}`);
        console.log('serviceEndTime:', `${serviceEndTime.getHours()}:${serviceEndTime.getMinutes()}`);
        const conflict = getConflicts(notAvailableSlots, serviceStartTime, serviceEndTime);
        console.log('conflict:', conflict);
        if (!Boolean(conflict)) {
            workingTimes.push(
                `${serviceStartTime.getHours()}:${
                    serviceStartTime.getMinutes() < 10 ? '0' : ''
                }${serviceStartTime.getMinutes()}`,
            );
            serviceStartTime = addMinutes(serviceStartTime, TIME_PERIOD);
            serviceEndTime = addMinutes(serviceEndTime, TIME_PERIOD);
        } else {
            notAvailableSlots = notAvailableSlots?.filter(
                (slot) =>
                    !(
                        slot.notAvailableTimeStart === conflict?.notAvailableTimeStart &&
                        slot.notAvailableTimeEnd === conflict.notAvailableTimeEnd
                    ),
            );
            serviceStartTime = conflict?.notAvailableTimeEnd!;
            serviceEndTime = addMinutes(serviceStartTime, Number(inputserviceDuration) + Number(inputserviceMargin));
        }
    }
    return workingTimes;
};
