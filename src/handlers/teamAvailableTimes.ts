import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { addMinutes, addDays, format } from 'date-fns';
import { isArray } from 'lodash';
import {
    TeamAvailableTimeOutput,
    QueryGetTeamAvailableTimeArgs,
    TeamServiceTimePrice,
    FirstAvailableTime,
} from '../../generated/graphql';
import { ITeamAppointmentsTable, ITeamBlockTimeTable, ITeamServiceTable } from '../dynamodb/types';
import { AWSError } from 'aws-sdk';
import { getTeamWorkingDay } from '../dynamodb/getTeamWorkingDay';
import { getTeamBlockTimes } from '../dynamodb/getTeamBlockTimes';
import { getTeamAppointmentsByDateTeamCode } from '../dynamodb/getTeamAppointments';
import { getTeamServices } from '../dynamodb/getTeamServices';
const TIME_PERIOD = 15;
type NotAvailableTimeSlots = Array<{ notAvailableTimeStart: Date; notAvailableTimeEnd: Date }>;

const todayDate = new Date(new Date().toLocaleString('en', { timeZone: 'Australia/Melbourne' }));
const today = format(todayDate, 'yyyy-MM-dd');

export const handler: AppSyncResolverHandler<
    QueryGetTeamAvailableTimeArgs,
    TeamAvailableTimeOutput[] | undefined
> = async (
    event: AppSyncResolverEvent<QueryGetTeamAvailableTimeArgs>,
): Promise<TeamAvailableTimeOutput[] | undefined> => {
    console.log('event.arguments:', event.arguments);
    if (!(event.arguments?.input && event.arguments?.input.length)) {
        // appDate is not passed
        return [
            {
                avaliableTimes: [],
            },
        ];
    }

    return (await Promise.all(
        event.arguments?.input.map(async (i) => {
            if (!i || !i.date || !i.duration || !i.margin || !i.teamCode) {
                return [];
            }
            const avaliableTimes = await getTeamAvailableTime(i.date, i.duration, i.margin, i.teamCode);
            return {
                avaliableTimes: isArray(avaliableTimes) ? avaliableTimes : [],
                firstAvailableTime:
                    isArray(avaliableTimes) && Boolean(avaliableTimes?.length)
                        ? undefined
                        : ((await getTeamAvailableTime(
                              i.date,
                              i.duration,
                              i.margin,
                              i?.teamCode,
                              [],
                          )) as FirstAvailableTime),
                teamCode: i.teamCode,
            };
        }),
    )) as TeamAvailableTimeOutput[];
};

const getTeamAvailableTime = async (
    inputDateParam: string,
    inputserviceDuration: number,
    inputserviceMargin: number,
    inputTeamCode: string,
    firstAvailableTime?: string[],
): Promise<string[] | undefined | FirstAvailableTime> => {
    const lookingForFirstAvailable = Boolean(firstAvailableTime);
    console.log('lookingForFirstAvailable:', lookingForFirstAvailable);

    if (lookingForFirstAvailable && firstAvailableTime && Boolean(firstAvailableTime.length)) {
        console.log('firstAvailableTime is found:', firstAvailableTime);
        return { avaliableTimes: firstAvailableTime, date: inputDateParam };
    }

    let inputDate = inputDateParam;
    if (lookingForFirstAvailable) {
        inputDate = format(addDays(new Date(inputDateParam), 1), 'yyyy-MM-dd');
    }
    console.log('inputDate:', inputDate);

    try {
        const teamWorkingDay = await getTeamWorkingDay(inputDate, inputTeamCode);
        console.log('teamWorkingDay:', teamWorkingDay);
        if (!(teamWorkingDay && teamWorkingDay.workingDay)) {
            // not a working day
            return !lookingForFirstAvailable
                ? []
                : getTeamAvailableTime(
                      inputDate,
                      inputserviceDuration,
                      inputserviceMargin,
                      inputTeamCode,
                      firstAvailableTime,
                  );
        }
        const teamBlockTimes = await getTeamBlockTimes(inputDate, inputTeamCode);
        console.log('teamBlockTimes:', teamBlockTimes);

        if (teamBlockTimes?.isBlockedWholeDay) {
            return !lookingForFirstAvailable
                ? []
                : getTeamAvailableTime(
                      inputDate,
                      inputserviceDuration,
                      inputserviceMargin,
                      inputTeamCode,
                      firstAvailableTime,
                  );
        }

        const notTeamAvailableSlots: NotAvailableTimeSlots = await getNotTeamAvailableSlots(
            inputDate,
            teamBlockTimes,
            inputTeamCode,
        );
        console.log(
            'notAvailableSlots:',
            notTeamAvailableSlots.map(({ notAvailableTimeEnd, notAvailableTimeStart }) => ({
                notAvailableTimeStart: format(notAvailableTimeStart, 'HH:mm'),
                notAvailableTimeEnd: format(notAvailableTimeEnd, 'HH:mm'),
            })),
        );
        let workingTimes: Array<string> = [];

        for (const teamWorkingHour of teamWorkingDay.workingHours) {
            if (!teamWorkingHour) {
                return !lookingForFirstAvailable
                    ? []
                    : getTeamAvailableTime(
                          inputDate,
                          inputserviceDuration,
                          inputserviceMargin,
                          inputTeamCode,
                          firstAvailableTime,
                      );
            }
            // workingHour: 15:00-20:00
            console.log('teamWorkingHour:', teamWorkingHour);
            // eslint-disable-next-line prefer-const,
            let [startWorkingHours, endWorkingHours] = teamWorkingHour
                .split('-')
                .map((slot) => new Date(`${today}T` + slot));

            if (inputDate === today) {
                console.log('inputDate is today', todayDate);
                while (startWorkingHours < todayDate) {
                    console.log('startWorkingHours in the loop', startWorkingHours);

                    startWorkingHours = addMinutes(startWorkingHours, 15);
                }
            }
            console.log('startWorkingHours:', format(startWorkingHours, 'HH:mm'));
            console.log('endWorkingHours:', format(endWorkingHours, 'HH:mm'));
            if (!startWorkingHours || !endWorkingHours) {
                return !lookingForFirstAvailable
                    ? []
                    : getTeamAvailableTime(
                          inputDate,
                          inputserviceDuration,
                          inputserviceMargin,
                          inputTeamCode,
                          firstAvailableTime,
                      );
            }
            endWorkingHours = addMinutes(endWorkingHours, inputserviceMargin); //adding margin to the end of the slot

            workingTimes = [
                ...workingTimes,
                ...getWorkingHoursForWorkingHoursSlot(
                    startWorkingHours,
                    endWorkingHours,
                    inputserviceDuration,
                    inputserviceMargin,
                    notTeamAvailableSlots,
                ),
            ];
        }
        return !lookingForFirstAvailable
            ? workingTimes
            : getTeamAvailableTime(inputDate, inputserviceDuration, inputserviceMargin, inputTeamCode, workingTimes);
    } catch (error) {
        console.log('error:', error);
        throw error as AWSError;
    }
};

const checkForConflict = (appS: Date, appE: Date, serS: Date, serE: Date) => {
    if (
        (serE > appS && serS < appS) ||
        (serS === appS && serE === appE) ||
        (serS >= appS && serE < appE) ||
        (serS >= appS && serE <= appE) ||
        (serS < appE && serE > appE)
    ) {
        return true;
    }
    return false;
};

const getNotAvailableTimeSlotsTeamAppointments = (
    appointments: ITeamAppointmentsTable[] | undefined,
    teamCode: string,
    services: ITeamServiceTable[] | undefined,
) => {
    if (Boolean(appointments?.length) && Boolean(services?.length)) {
        return appointments?.map((appointment) => {
            const notAvailableTimeStart = new Date(`${today}T` + appointment.appTime);
            const serviceTimePrice = services
                ?.find(
                    (service) => service.code === (appointment.selectedService as { serviceCode: string }).serviceCode,
                )
                ?.timePrice.find(
                    (t: TeamServiceTimePrice) =>
                        t.code ===
                        (appointment.selectedService as { serviceTimePriceCode: string }).serviceTimePriceCode,
                )
                ?.team.find((t) => t.teamCode === teamCode);

            const notAvailableTimeEnd = addMinutes(
                new Date(`${today}T` + appointment.appTime),
                (serviceTimePrice?.duration ?? 0) + (serviceTimePrice?.margin ?? 0),
            );
            return {
                notAvailableTimeStart,
                notAvailableTimeEnd,
            };
        });
    }
    return [];
};

const getNotAvailableTimeSlotsTeamBlockTime = (teamBlockTimes: ITeamBlockTimeTable[]) => {
    if (Boolean(teamBlockTimes?.length)) {
        return teamBlockTimes?.map((teamBlockTime) => ({
            notAvailableTimeStart: new Date(`${today}T` + teamBlockTime.time.split('-')[0]),
            notAvailableTimeEnd: new Date(`${today}T` + teamBlockTime.time.split('-')[1]),
        }));
    }
    return [];
};

const getNotTeamAvailableSlots = async (
    inputDate: string,
    teamBlockTimes:
        | {
              isBlockedWholeDay: boolean;
              blockTime: ITeamBlockTimeTable[];
          }
        | undefined,
    teamCode: string,
) => {
    const teamAppointments = await getTeamAppointmentsByDateTeamCode(inputDate, teamCode);
    const services = await getTeamServices(['timePrice']);

    return [
        ...getNotAvailableTimeSlotsTeamBlockTime(teamBlockTimes?.blockTime ?? []),
        ...(getNotAvailableTimeSlotsTeamAppointments(teamAppointments, teamCode, services) ?? []),
    ];
};
type Conflict = {
    notAvailableTimeStart: Date;
    notAvailableTimeEnd: Date;
};
const getConflicts = (
    notAvailableSlots: NotAvailableTimeSlots,
    serviceStartTime: Date,
    serviceEndTime: Date,
): Conflict | undefined => {
    if (Boolean(notAvailableSlots.length)) {
        return notAvailableSlots?.find((slot) =>
            checkForConflict(slot.notAvailableTimeStart, slot.notAvailableTimeEnd, serviceStartTime, serviceEndTime),
        );
    }
};
const getWorkingHoursForWorkingHoursSlot = (
    startWorkingHours: Date,
    endWorkingHours: Date,
    inputserviceDuration: number,
    inputserviceMargin: number,
    notAvailableSlots: NotAvailableTimeSlots,
): Array<string> => {
    const workingTimes: Array<string> = [];
    let serviceStartTime = new Date(startWorkingHours);
    let serviceEndTime = addMinutes(
        new Date(startWorkingHours),
        Number(inputserviceDuration) + Number(inputserviceMargin),
    );
    console.log('serviceStartTime:', format(serviceStartTime, 'HH:mm'));
    console.log('serviceEndTime:', format(serviceEndTime, 'HH:mm'));
    while (serviceEndTime <= endWorkingHours) {
        console.log('serviceStartTime:', `${serviceStartTime.getHours()}:${serviceStartTime.getMinutes()}`);
        console.log('serviceEndTime:', `${serviceEndTime.getHours()}:${serviceEndTime.getMinutes()}`);
        const conflict = getConflicts(notAvailableSlots, serviceStartTime, serviceEndTime);
        console.log('conflict:', conflict);
        if (!Boolean(conflict)) {
            workingTimes.push(
                `${serviceStartTime.getHours()}:${
                    serviceStartTime.getMinutes() < 10 ? '0' : ''
                }${serviceStartTime.getMinutes()}`,
            );
            serviceStartTime = addMinutes(serviceStartTime, TIME_PERIOD);
            serviceEndTime = addMinutes(serviceEndTime, TIME_PERIOD);
        } else {
            notAvailableSlots = notAvailableSlots?.filter(
                (slot) =>
                    !(
                        slot.notAvailableTimeStart === conflict?.notAvailableTimeStart &&
                        slot.notAvailableTimeEnd === conflict.notAvailableTimeEnd
                    ),
            );
            serviceStartTime = conflict?.notAvailableTimeEnd!;
            serviceEndTime = addMinutes(serviceStartTime, Number(inputserviceDuration) + Number(inputserviceMargin));
        }
    }
    return workingTimes;
};
