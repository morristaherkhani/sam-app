import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { TeamAppointment, QueryGetTeamAppointmentsArgs } from '../../generated/graphql';
import { addMinutes, format } from 'date-fns';
import {
    getTeamAppointmentById,
    getTeamAppointmentByMobile,
    getTeamAppointmentsByStatus,
    getTeamAppointmentsByDateRangeTeamCode,
    getTeamAppointmentsByDateTeamCode,
} from '../dynamodb/getTeamAppointments';
import { ITeamAppointmentsTable } from '../dynamodb/types';
import { getClientByMobile } from '../dynamodb/getClient';
import { AWSError } from 'aws-sdk';
import { getTeamServices } from '../dynamodb/getTeamServices';

export const handler: AppSyncResolverHandler<QueryGetTeamAppointmentsArgs, TeamAppointment[] | undefined> = async (
    event: AppSyncResolverEvent<QueryGetTeamAppointmentsArgs>,
): Promise<TeamAppointment[] | undefined> => {
    const id = event.arguments.input?.id;
    const appDates = event.arguments.input?.appDates;
    const mobile = event.arguments.input?.mobile;
    const appintmentStatus = event.arguments.input?.appintmentStatus;
    const teamCode = event.arguments.input?.teamCode;
    try {
        if (!event || !event.arguments) {
            return undefined;
        }
        let teamAppointments;
        if (mobile) {
            teamAppointments = await getTeamAppointmentByMobile(mobile);
        }
        if (appintmentStatus) {
            teamAppointments = await getTeamAppointmentsByStatus(appintmentStatus);
        }
        if (id) {
            teamAppointments = [await getTeamAppointmentById(id)];
        }
        if (appDates && teamCode) {
            if (appDates.length > 1) {
                const startDate = new Date(appDates[0] as string);
                const endDate = new Date(appDates[1] as string);
                teamAppointments = await getTeamAppointmentsByDateRangeTeamCode(
                    format(startDate, 'yyyy-MM-dd'),
                    format(endDate, 'yyyy-MM-dd'),
                    teamCode,
                );
            }
            if (appDates.length === 1) {
                const startDate = new Date(appDates[0] as string);
                teamAppointments = await getTeamAppointmentsByDateTeamCode(format(startDate, 'yyyy-MM-dd'), teamCode);
            }
        }
        if (!Boolean(teamAppointments)) {
            return;
        }
        const services = await getTeamServices(['timePrice']);
        // console.log('services:', services);
        return await Promise.all(
            teamAppointments!
                .filter((app) => Boolean(app))
                .map(async (app) => {
                    const { mobile, ...appData } = app as ITeamAppointmentsTable;
                    const client = await getClientByMobile(mobile);
                    const service = services?.find((service) => service.code === appData.selectedService.serviceCode);
                    const serviceTimePrice = service?.timePrice.find(
                        (timePrice) => timePrice.code === appData.selectedService.serviceTimePriceCode,
                    );
                    return {
                        ...appData,
                        appEndTime: format(
                            addMinutes(new Date('1970-01-01T' + appData.appTime), serviceTimePrice?.duration ?? 0),
                            'HH:mm',
                        ),
                        client: {
                            ...client,
                        },
                        selectedService: {
                            service: {
                                code: service?.code,
                                name: service?.name,
                                htmlScripts: [],
                                timePrice: [],
                                displayOrder: service?.displayOrder,
                            },
                            serviceTimePrice,
                        },
                    };
                }),
        );
    } catch (error) {
        throw error as AWSError;
    }
};
