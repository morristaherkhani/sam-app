import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { AWSError } from 'aws-sdk';
export const S3_BUCKET_NAME = 'lashdiamond-files';
import { S3Client, DeleteObjectsCommand } from '@aws-sdk/client-s3';
type Files = { files: [string] };
export const handler: AppSyncResolverHandler<Files, string[]> = async (
    event: AppSyncResolverEvent<Files>,
): Promise<string[]> => {
    try {
        const files = event.arguments.files;
        console.log('files:', files);
        console.log('abc:', {
            Objects: files.map((file) => ({
                Key: file,
            })),
        });
        const client = new S3Client();
        const result = await client.send(
            new DeleteObjectsCommand({
                Bucket: S3_BUCKET_NAME,
                Delete: {
                    Objects: files.map((file) => ({
                        Key: file,
                    })),
                },
            }),
        );
        console.log('result:', result);
        return result.Deleted?.map((deleted) => deleted.Key!) ?? [];
    } catch (error) {
        console.log('error:', error);

        throw error as AWSError;
    }
};
