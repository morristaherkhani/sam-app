import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { AWSError } from 'aws-sdk';
import { GooglePlace, MutationUpdateGooglePlaceArgs } from '../../generated/graphql';
import { deleteGooglePlace } from '../dynamodb/deleteGooglePlace';
import { putGooglePlace } from '../dynamodb/putGooglePlace';

export const handler: AppSyncResolverHandler<MutationUpdateGooglePlaceArgs, GooglePlace | undefined> = async (
    event: AppSyncResolverEvent<MutationUpdateGooglePlaceArgs>,
): Promise<GooglePlace | undefined> => {
    const googlePlace = event.arguments.input;
    if (!googlePlace) {
        return;
    }
    const { rating, user_ratings_total, reviews } = googlePlace;
    try {
        await deleteGooglePlace('1');
        await putGooglePlace({
            id: '1',
            rating,
            user_ratings_total,
            reviews: reviews ?? [],
        });

        return googlePlace;
    } catch (error) {
        console.log('error:', error);
        throw error as AWSError;
    }
};
