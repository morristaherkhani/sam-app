import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { QueryGetServiceArgs, S3File, Service } from '../../generated/graphql';
import { AWSError } from 'aws-sdk';
import { getFilesS3 } from '../files/getFilesS3';
import { getServiceByCode } from '../dynamodb/getServiceByCode';

export const handler: AppSyncResolverHandler<QueryGetServiceArgs, Service | undefined> = async (
    event: AppSyncResolverEvent<QueryGetServiceArgs>,
): Promise<Service | undefined> => {
    try {
        if (!event || !event.arguments || !event.arguments.code) {
            return undefined;
        }
        const requestedFields = event.arguments.requestedFields;
        const code = event.arguments.code;

        const service = await getServiceByCode(code, [
            ...(requestedFields?.includes('timePrice') ? ['timePrice'] : []),
            ...(requestedFields?.includes('htmlScripts') ? ['htmlScripts'] : []),
        ]);
        if (requestedFields?.includes('homepage-image') && service) {
            const files: S3File[] = await getFilesS3(`services/${service.code}/homepage-image`);
            return {
                ...service,
                images: [{ location: 'homepage-image', url: files && files[0]?.url }],
            };
        }
        if (requestedFields?.includes('bookingpage-image') && service) {
            const files: S3File[] = await getFilesS3(`services/${service.code}/bookingpage-image`);
            return {
                ...service,
                images: [{ location: 'bookingpage-image', url: files && files[0]?.url }],
            };
        }
        return service;
    } catch (error) {
        throw error as AWSError;
    }
};
