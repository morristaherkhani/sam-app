import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { TeamAppointmentOutput, MutationAddTeamAppointmentArgs } from '../../generated/graphql';
import { putClient } from '../dynamodb/putClient';
import { IClientTable, ISelectedService } from '../dynamodb/types';
import { putTeamAppointment } from '../dynamodb/putTeamAppointment';
import { AWSError } from 'aws-sdk';

export const handler: AppSyncResolverHandler<
    MutationAddTeamAppointmentArgs,
    TeamAppointmentOutput | undefined
> = async (event: AppSyncResolverEvent<MutationAddTeamAppointmentArgs>): Promise<TeamAppointmentOutput | undefined> => {
    if (!event || !event.arguments || !event.arguments.input) {
        return undefined;
    }
    try {
        const {
            teamCode,
            firstName,
            lastName,
            mobile,
            address,
            email,
            instagram,
            appointmentStatus,
            appDate,
            selectedService,
            appTime,
            clientComment,
            description,
            allergic,
            id,
            submitionDateTime,
        } = event.arguments.input;
        const client: IClientTable = {
            mobile: mobile as string,
            firstName: firstName as string,
            address: address as string | undefined,
            email: email as string | undefined,
            instagram: instagram as string | undefined,
            lastName: lastName as string | undefined,
            allergic: allergic as boolean | undefined,
        };
        console.log('client is added');
        await putClient(client);
        const teamAppointment = {
            teamCode,
            appointmentStatus: appointmentStatus as string,
            mobile: mobile as string,
            appDate: appDate as string,
            selectedService: selectedService as ISelectedService,
            appTime: appTime as string,
            clientComment: clientComment as string | undefined,
            description: description as string | undefined,
            id: id as string,
            submitionDateTime: submitionDateTime as string,
        };
        await putTeamAppointment(teamAppointment);
        return { id, mobile: mobile!, teamCode };
    } catch (error) {
        throw error as AWSError;
    }
};
