import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { QueryGetTeamServiceArgs, S3File, TeamService } from '../../generated/graphql';
import { AWSError } from 'aws-sdk';
import { getFilesS3 } from '../files/getFilesS3';
import { getTeamServiceByCode } from '../dynamodb/getTeamServiceByCode';

export const handler: AppSyncResolverHandler<QueryGetTeamServiceArgs, TeamService | undefined> = async (
    event: AppSyncResolverEvent<QueryGetTeamServiceArgs>,
): Promise<TeamService | undefined> => {
    try {
        if (!event || !event.arguments || !event.arguments.code) {
            return undefined;
        }
        const requestedFields = event.arguments.requestedFields;
        const code = event.arguments.code;

        const teamService = await getTeamServiceByCode(code, [
            ...(requestedFields?.includes('timePrice') ? ['timePrice'] : []),
            ...(requestedFields?.includes('htmlScripts') ? ['htmlScripts'] : []),
        ]);
        if (requestedFields?.includes('homepage-image') && teamService) {
            const files: S3File[] = await getFilesS3(`services/${teamService.code}/homepage-image`);
            return {
                ...teamService,
                images: [{ location: 'homepage-image', url: files && files[0]?.url }],
            };
        }
        if (requestedFields?.includes('bookingpage-image') && teamService) {
            const files: S3File[] = await getFilesS3(`services/${teamService.code}/bookingpage-image`);
            return {
                ...teamService,
                images: [{ location: 'bookingpage-image', url: files && files[0]?.url }],
            };
        }
        return teamService;
    } catch (error) {
        throw error as AWSError;
    }
};
