import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { AWSError } from 'aws-sdk';
import { S3File } from '../../generated/graphql';
import { getFilesS3 } from '../files/getFilesS3';
type S3Path = { s3Path: string };

export const handler: AppSyncResolverHandler<S3Path, S3File[]> = async (
    event: AppSyncResolverEvent<S3Path>,
): Promise<S3File[]> => {
    try {
        const s3Path = event.arguments?.s3Path;
        const result = await getFilesS3(s3Path);
        return result;
    } catch (error) {
        console.log('error:', error);

        throw error as AWSError;
    }
};
