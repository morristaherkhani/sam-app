import { AppSyncResolverHandler } from 'aws-lambda';
import AWS from 'aws-sdk';
import { putClientReview } from '../dynamodb/putClientReview';

const tableName = 'ClientsTable'; // Replace with your table name

export const handler: AppSyncResolverHandler<undefined, undefined> = async (): Promise<undefined> => {
    const docClient = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: tableName,
        FilterExpression: 'askedForReview = :value',
        ExpressionAttributeValues: {
            ':value': true,
        },
    };

    try {
        const result = await docClient.scan(params).promise();
        console.log('result1:', result);
        if (result && result?.Items) {
            for (const item of result?.Items) {
                console.log('before');
                const updateResult = await putClientReview(item.mobile, '11/09/2024', true);
                console.log('result:', updateResult);
            }
        }
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
