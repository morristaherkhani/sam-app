import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { Appointment, QueryGetAppointmentsArgs } from '../../generated/graphql';
import { addMinutes, format } from 'date-fns';
import {
    getAppointmentById,
    getAppointmentsByDate,
    getAppointmentByMobile,
    getAppointmentsByStatus,
} from '../dynamodb/getAppointments';
import { IAppointmentsTable } from '../dynamodb/types';
import { getClientByMobile } from '../dynamodb/getClient';
import { getServices } from '../dynamodb/getServices';
import { AWSError } from 'aws-sdk';

export const handler: AppSyncResolverHandler<QueryGetAppointmentsArgs, Appointment[] | undefined> = async (
    event: AppSyncResolverEvent<QueryGetAppointmentsArgs>,
): Promise<Appointment[] | undefined> => {
    const id = event.arguments.input?.id;
    const appDates = event.arguments.input?.appDates;
    const mobile = event.arguments.input?.mobile;
    const appintmentStatus = event.arguments.input?.appintmentStatus;

    try {
        if (!event || !event.arguments) {
            return undefined;
        }
        let appointments;
        if (mobile) {
            appointments = await getAppointmentByMobile(mobile);
        }
        if (appintmentStatus) {
            appointments = await getAppointmentsByStatus(appintmentStatus);
        }
        if (id) {
            appointments = [await getAppointmentById(id)];
        }
        if (appDates) {
            if (appDates.length > 1) {
                const startDate = new Date(appDates[0] as string);
                const endDate = new Date(appDates[1] as string);
                startDate.setDate(startDate.getDate() + 1);
                while (startDate < endDate) {
                    appDates.push(format(startDate, 'yyyy-MM-dd'));
                    startDate.setDate(startDate.getDate() + 1);
                }
            }
            const promises = appDates.map((appDate) => (appDate ? getAppointmentsByDate(appDate) : []));
            appointments = (await Promise.all(promises)).reduce(
                (apps, app) => [
                    ...(apps as unknown[] as IAppointmentsTable[]),
                    ...(app as unknown[] as IAppointmentsTable[]),
                ],
                [] as IAppointmentsTable[],
            );
        }
        if (!Boolean(appointments)) {
            return;
        }
        const services = await getServices(['timePrice']);
        // console.log('services:', services);
        return await Promise.all(
            appointments!
                .filter((app) => Boolean(app))
                .map(async (app) => {
                    const { mobile, ...appData } = app as IAppointmentsTable;
                    const client = await getClientByMobile(mobile);
                    const service = services?.find((service) => service.code === appData.selectedService.serviceCode);
                    const serviceTimePrice = service?.timePrice.find(
                        (timePrice) => timePrice.code === appData.selectedService.serviceTimePriceCode,
                    );
                    return {
                        ...appData,
                        appEndTime: format(
                            addMinutes(new Date('1970-01-01T' + appData.appTime), serviceTimePrice?.duration ?? 0),
                            'HH:mm',
                        ),
                        client: {
                            ...client,
                        },
                        selectedService: {
                            service: {
                                code: service?.code,
                                name: service?.name,
                                htmlScripts: [],
                                timePrice: [],
                                displayOrder: service?.displayOrder,
                            },
                            serviceTimePrice,
                        },
                    };
                }),
        );
    } catch (error) {
        throw error as AWSError;
    }
};
