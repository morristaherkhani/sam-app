import { AppSyncResolverHandler } from 'aws-lambda';
import { ClientsForReviewFollow } from '../../generated/graphql';
import { AWSError } from 'aws-sdk';
import { getAppointmentByMobile, getAppointmentsByDateRange } from '../dynamodb/getAppointments';
import { addDays, format } from 'date-fns';
import { AWSDATE_FORMAT } from '../dynamodb/types';
import { getClientByMobile } from '../dynamodb/getClient';

export const handler: AppSyncResolverHandler<undefined, ClientsForReviewFollow[] | undefined> = async (): Promise<
    ClientsForReviewFollow[] | undefined
> => {
    try {
        const lastWeekAppointments = await getAppointmentsByDateRange(
            format(addDays(new Date(), -7), AWSDATE_FORMAT),
            format(new Date(), AWSDATE_FORMAT),
        );

        if (lastWeekAppointments) {
            const promises = lastWeekAppointments.map(async (appointment) => {
                const client = await getClientByMobile(appointment.mobile);
                if (!client?.askedForFollow) {
                    const clientAppointments = await getAppointmentByMobile(appointment.mobile);
                    if (clientAppointments) {
                        return {
                            mobile: appointment.mobile,
                            appointments: clientAppointments.map((app) => ({
                                id: app.id,
                                appDate: app.appDate,
                                appTime: app.appTime,
                                mobile: app.mobile,
                                service:
                                    app.selectedService.serviceCode + ' ' + app.selectedService.serviceTimePriceCode,
                            })),
                            firstName: client?.firstName,
                            lastName: client?.lastName,
                        };
                    }
                }
            });
            const result = (await Promise.all(promises)).filter((client) => !!client);
            return result;
        }
    } catch (error) {
        throw error as AWSError;
    }
};
