import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { QueryGetTeamServicesArgs, S3File, TeamService } from '../../generated/graphql';
import { AWSError } from 'aws-sdk';
import { getFilesS3 } from '../files/getFilesS3';
import { getTeamServices } from '../dynamodb/getTeamServices';

export const handler: AppSyncResolverHandler<QueryGetTeamServicesArgs, TeamService[] | undefined> = async (
    event: AppSyncResolverEvent<QueryGetTeamServicesArgs>,
): Promise<TeamService[] | undefined> => {
    try {
        if (!event || !event.arguments) {
            return undefined;
        }
        const requestedFields = event.arguments.requestedFields;

        const teamServices = await getTeamServices([
            ...(requestedFields?.includes('timePrice') ? ['timePrice'] : []),
            ...(requestedFields?.includes('htmlScripts') ? ['htmlScripts'] : []),
        ]);
        if (requestedFields?.includes('homepage-image')) {
            return await Promise.all(
                teamServices?.map(async (teamService) => {
                    const files: S3File[] = await getFilesS3(`services/${teamService.code}/homepage-image`);
                    return {
                        ...teamService,
                        images: [{ location: 'homepage-image', url: files && files[0]?.url }],
                    };
                }) ?? [],
            );
        }
        if (requestedFields?.includes('bookingpage-image')) {
            return await Promise.all(
                teamServices?.map(async (teamService) => {
                    const files: S3File[] = await getFilesS3(`services/${teamService.code}/bookingpage-image`);
                    return {
                        ...teamService,
                        images: [{ location: 'bookingpage-image', url: files && files[0]?.url }],
                    };
                }) ?? [],
            );
        }
        return teamServices;
    } catch (error) {
        throw error as AWSError;
    }
};
