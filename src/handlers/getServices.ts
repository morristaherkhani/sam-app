import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { QueryGetServicesArgs, S3File, Service } from '../../generated/graphql';
import { getServices } from '../dynamodb/getServices';
import { AWSError } from 'aws-sdk';
import { getFilesS3 } from '../files/getFilesS3';

export const handler: AppSyncResolverHandler<QueryGetServicesArgs, Service[] | undefined> = async (
    event: AppSyncResolverEvent<QueryGetServicesArgs>,
): Promise<Service[] | undefined> => {
    try {
        if (!event || !event.arguments) {
            return undefined;
        }
        const requestedFields = event.arguments.requestedFields;

        const services = await getServices([
            ...(requestedFields?.includes('timePrice') ? ['timePrice'] : []),
            ...(requestedFields?.includes('htmlScripts') ? ['htmlScripts'] : []),
        ]);
        if (requestedFields?.includes('homepage-image')) {
            return await Promise.all(
                services?.map(async (service) => {
                    const files: S3File[] = await getFilesS3(`services/${service.code}/homepage-image`);
                    return {
                        ...service,
                        images: [{ location: 'homepage-image', url: files && files[0]?.url }],
                    };
                }) ?? [],
            );
        }
        if (requestedFields?.includes('bookingpage-image')) {
            return await Promise.all(
                services?.map(async (service) => {
                    const files: S3File[] = await getFilesS3(`services/${service.code}/bookingpage-image`);
                    return {
                        ...service,
                        images: [{ location: 'bookingpage-image', url: files && files[0]?.url }],
                    };
                }) ?? [],
            );
        }
        return services;
    } catch (error) {
        throw error as AWSError;
    }
};
