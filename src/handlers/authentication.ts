import { AppSyncResolverEvent, AppSyncResolverHandler } from 'aws-lambda';
import { getSettingsByType } from '../dynamodb/getSettings';
import { AuthenticationOutput, QueryAuthenticateArgs } from '../../generated/graphql';

export const handler: AppSyncResolverHandler<
    QueryAuthenticateArgs,
    AuthenticationOutput
> = async (event: AppSyncResolverEvent<QueryAuthenticateArgs>): Promise<AuthenticationOutput> => {
    if (!event.arguments.input?.username || !event.arguments.input?.password) {
        return { isAuthenticated: false };
    }
    const { username, password } = event.arguments.input;
    const admins = await getSettingsByType('admins');

    return {
        isAuthenticated: Boolean(
            admins?.settings.find((setting) => setting.username === username && setting.password === password),
        ),
    };
};
