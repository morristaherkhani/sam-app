export const S3_BUCKET_NAME = 'lashdiamond-files';

export const getUrlFromBucket = (fileName: string) => `https://${S3_BUCKET_NAME}.s3.ap-southeast-2.amazonaws.com/${fileName}`;
