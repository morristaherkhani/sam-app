import { google } from 'googleapis';

const SCOPES = ['https://www.googleapis.com/auth/calendar.events', 'https://www.googleapis.com/auth/calendar'];
const CALENDAR_ID = 'info@eyelashextensionstat.com.au';
const credential = {
    type: 'service_account',
    project_id: 'eyelash-extensions-tat-400204',
    private_key_id: '8d47bd0a9eee842c7056e2d4cd101566925bb2ea',
    private_key:
        '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC/hqAk71RfPlfd\nSoUx1b7Hho3jN9Dzeit0pvOJzX+rK9ZKjoj2DZV8DXlBPtOugDYxeKy85p8Z8cuw\n8/JQoce4GqD7leUoHfq3Q8g8pV/qbNzVR8kGgGW69m0g+fEJtkzGGnV7y/LA0W57\nwWcYitsw0nLKULo6a5ZFbMstCcP5bR56Vxky3FZfnCwOI037eati29fTgSKmZV/e\naRxR4LE6tH+aL38lFodecQQzEOXzVo4KjK/nziaDp6p9Sx/czmqirrgND8FVhX8e\nOIH+itVWl//bGVxc2ZxnjIPE+BqH0eycbZuW//U2V1G83MxCT3FSOXq15m1/0LlM\ni7xPenivAgMBAAECggEAEDMLrjlnT6upvzKzjlyvshehk6jfwwrqdweR924Ku4h4\ngROKO77iuGc5V3lAFUqMiuWpPUxdA6hDHBieE7ot9tabFtHTBprL3x6xZokZp5iF\nRdg4Wr+XGy0HBkWDC4PAUvc08v9z7QwpxfeR8pj4DiqWq/cPsVZH6QpZKwTgRGM+\nY52i0dYLCnaXnp4eBIznUWiPMSmsg95XhREBln0XaKgK8J0BcoVQtzjQKj7bCUBM\nUQls3w3PGN0DMg0LWKK8BCi1t2qKB3pGBXGz4jbcJWjEYptGqLFJJeEhtYz9GMFj\nOfZvoeCNd8RNnNhYXTuofh17wAttyH4nWM4Sf9kZpQKBgQD9/b4EpSWJBNGomP65\nFWpQiQ/UJ6bbVAQg5DzqaWqRuBkdbvYntKKQPHvnPGcP1m2is/I7eJklct60EkNm\nWOGdOFnwJZgSJ1SUYA7bmwmH5LNjHuwLu3PzukLkxrjKz1Nmbg0bMBtMax+pjQzg\n9ezjVAXDTFOEQBX4Z2AJIA16hQKBgQDBCmjMiFEmwcXiORwGGc/ISkAfFgadHsD/\nBpZDMDylOLlf98tG6YhcEFhTvdWU7awbyrgjq0Rz/d42M5l5qLa3EVV0zrWWYR/o\nAgqVntLWW3mlhALUakHWAW2CREBnwV6qmnXy/ZRqdntxm2W8Gnu0zgq+MzNV78BN\niDFT8bh+owKBgQDeh5ETKsoPGUzDpYAPEuc5tkxK+Y6kPSD83FtOmZuIHj7K3ewG\n76k6K/P2raIyaxFVaslBN7Ed8V2TcSjiI99Pyw5pa9anmRPuh5ac62HXURWmM3Jl\n+Q9nK/rty/odnrYYWTVca7KPKo/sEiuFcZHhvjzqVhYaGbfmElQ+ynlkrQKBgC/Z\nmbKhU6RMwfVafKTSfxvBs/VAq3EE74gHpPWQnmfCEMlGDgBM4P0ZHBvd4XRiGZhl\n/pDw4i3yTsOWtMNgW7IzPMCQgJh/CL6kyuTdkpuJ3zR07ZKjqCAZCIES0/gkCYg2\nc2hBDSn6poy5nVUsJ1/g9u7AmEgD/uH5EWkicL4LAoGAK1AhW0YEDyPNV3hn/5ik\nDzbWNXBmAvFaWmLiE4XX9vGIH0kV5HO+aw/dSU50irqFPQY9vU/t30g9bhxBOtuq\nfby9jEUYs4hhHPIA3yOpWgL8/PWY7ThuH1dksRnDBA8EvZxVH4JzbVw/JTg6IlqH\nyS0k/EnvHop0VglvAonRxPQ=\n-----END PRIVATE KEY-----\n',
    client_email: 'eyelashextensionstat@eyelash-extensions-tat-400204.iam.gserviceaccount.com',
    client_id: '105527318747494176850',
    auth_uri: 'https://accounts.google.com/o/oauth2/auth',
    token_uri: 'https://oauth2.googleapis.com/token',
    auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
    client_x509_cert_url:
        'https://www.googleapis.com/robot/v1/metadata/x509/eyelashextensionstat%40eyelash-extensions-tat-400204.iam.gserviceaccount.com',
    universe_domain: 'googleapis.com',
};

const insertEventIntoCalendar = async () => {
    try {
        console.log('Inside the functions');
        const jwt = new google.auth.JWT(credential.client_email, undefined, credential.private_key, SCOPES);
        console.log('2');
        const calendar = google.calendar({ version: 'v3', auth: jwt });
        console.log(
            'calendar:',
            await calendar.calendarList.get({
                calendarId: CALENDAR_ID,
                auth: jwt,
            }),
        );
        const res = await calendar.events
            .list({
                calendarId: CALENDAR_ID,
                timeMin: new Date().toISOString(),
                maxResults: 10,
                singleEvents: true,
                orderBy: 'startTime',
                auth: jwt,
            })
            .then((response) => console.log('response:', response.data));
        console.log('res:', res);
    } catch (error) {
        console.log('error:', error);
    }
};

export { insertEventIntoCalendar };
