import AWS, { AWSError } from 'aws-sdk';
import { ITeamWorkingDays } from './types';

const getTeamWorkingDay = async (date: string, teamCode: string): Promise<ITeamWorkingDays> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const dayOfWeek = new Date(date).toLocaleString('en-au', { weekday: 'long' });
        const params = {
            TableName: 'TeamWorkingDaysTable',
            KeyConditionExpression: '#pk = :pk AND #sk = :sk',
            ExpressionAttributeNames: {
                '#pk': 'teamCode', // Replace with your partition key name
                '#sk': 'name', // Replace with your sort key name
            },
            ExpressionAttributeValues: {
                ':pk': teamCode,
                ':sk': dayOfWeek,
            },
        };
        return ((await docClient.query(params).promise()).Items as unknown as ITeamWorkingDays[])[0];
    } catch (error) {
        console.log('error happened here:', error);
        throw error as AWSError;
    }
};

export { getTeamWorkingDay };
