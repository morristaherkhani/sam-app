import AWS, { AWSError } from 'aws-sdk';

const deleteGooglePlace = async (id: string) => {
    console.log('id in delete:', id);
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'GooglePlaceTable',
            Key: {
                id,
            },
        };
        const result = await docClient.delete(params).promise();
        return result;
    } catch (error) {
        throw error as AWSError;
    }
};

export { deleteGooglePlace };
