import AWS, { AWSError } from 'aws-sdk';
import { ITeamWorkingDays } from './types';

const getTeamWorkingDays = async () => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'TeamWorkingDaysTable',
        };
        return (await docClient.scan(params).promise()).Items as unknown[] as ITeamWorkingDays[];
    } catch (error) {
        throw error as AWSError;
    }
};

export { getTeamWorkingDays };
