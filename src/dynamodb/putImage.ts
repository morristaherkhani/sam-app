import AWS, { AWSError } from 'aws-sdk';
import { IImage } from './types';

const putImage = async (image: IImage) => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'ImagesTable',
            Item: image,
        };

        const result = await docClient.put(params).promise();
        console.log('result in data:', result);
        return result;
    } catch (error) {
        throw error as AWSError;
    }
};

export { putImage };
