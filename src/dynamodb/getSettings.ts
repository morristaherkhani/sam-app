import AWS from 'aws-sdk';
import { ISettingsTable } from './types';
const getSettingsByType = async (type: 'admins' | 'workingHours'): Promise<ISettingsTable | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'SettingsTable',
            Key: {
                type: type,
            },
        };
        return (await docClient.get(params).promise()).Item as unknown as ISettingsTable;
    } catch (error) {
        console.log('error in getSettingsByType:', error);
    }
};

export { getSettingsByType };
