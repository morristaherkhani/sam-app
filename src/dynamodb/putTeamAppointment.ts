import AWS, { AWSError } from 'aws-sdk';
import { ITeamAppointmentsTable } from './types';

const putTeamAppointment = async (teamappointment: ITeamAppointmentsTable) => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const paramsAppointment = {
            TableName: 'TeamAppointmentsTable',
            Item: teamappointment,
        };
        await docClient.put(paramsAppointment).promise();
    } catch (error) {
        console.log('error:', error);
        throw error as AWSError;
    }
};

export { putTeamAppointment };
