import AWS, { AWSError } from 'aws-sdk';
import { IServiceTable } from './types';
const getServices = async (attributesToGet: string[] = []): Promise<IServiceTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'ServicesTable',
            AttributesToGet: ['name', 'code', 'displayOrder', ...attributesToGet],
        };

        return ((await docClient.scan(params).promise()).Items as unknown[] as IServiceTable[]).sort(
            (a, b) => a.displayOrder - b.displayOrder,
        );
    } catch (error) {
        throw error as AWSError;
    }
};

export { getServices };
