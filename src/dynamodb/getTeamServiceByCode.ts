import AWS, { AWSError } from 'aws-sdk';
import { ITeamServiceTable } from './types';
const getTeamServiceByCode = async (
    code: string,
    attributesToGet: string[] = [],
): Promise<ITeamServiceTable | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'TeamServicesTable',
            Key: {
                code,
            },
            AttributesToGet: ['name', 'code', 'displayOrder', ...attributesToGet],
        };
        return (await docClient.get(params).promise()).Item as unknown as ITeamServiceTable;
    } catch (error) {
        throw error as AWSError;
    }
};

export { getTeamServiceByCode };
