import AWS, { AWSError } from 'aws-sdk';
import { IClientTable } from './types';

const putClient = async (client: IClientTable) => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'ClientsTable',
            Item: client,
        };

        const result = await docClient.put(params).promise();
        return result;
    } catch (error) {
        throw error as AWSError;
    }
};

export { putClient };
