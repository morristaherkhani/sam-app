import { AppointmentStatus, GoogleReviewInput } from '../../generated/graphql';

export interface ISelectedService {
    serviceCode: string;
    serviceTimePriceCode: string;
}

export interface IClientTable {
    mobile: string;
    firstName?: string;
    address?: string;
    email?: string;
    instagram?: string;
    lastName?: string;
    allergic?: boolean;
    askedForReview?: boolean;
    askedForFollow?: boolean;
}
export interface IClientReviewTable {
    mobile: string;
    isGiven: boolean;
    sentDates: string[];
}
export interface IAppointmentsTable {
    id: string;
    selectedService: ISelectedService;
    appDate: string;
    appTime: string;
    appointmentStatus: AppointmentStatus;
    clientComment?: string;
    description?: string;
    submitionDateTime: string;
    mobile: string;
}

export interface ITeamAppointmentsTable {
    teamCode: string;
    id: string;
    selectedService: ISelectedService;
    appDate: string;
    appTime: string;
    appointmentStatus: AppointmentStatus;
    clientComment?: string;
    description?: string;
    submitionDateTime: string;
    mobile: string;
}
export interface IServiceTable {
    name: string;
    code: string;
    timePrice: IServiceTimePrice[];
    htmlScripts: string[];
    displayOrder: number;
}
export interface IServiceTimePrice {
    code: string;
    title: string;
    price: number;
    duration: number;
    margin: number;
}

export interface ITeamServiceTable {
    name: string;
    code: string;
    timePrice: ITeamServiceTimePrice[];
    htmlScripts: string[];
    displayOrder: number;
}
export interface ITeamServiceTimePrice {
    code: string;
    title: string;
    price: number;
    duration: number;
    margin: number;
    team: ITeamServiceTime[];
}
export interface ITeamServiceTime {
    teamCode: string;
    duration: number;
    margin: number;
}

export interface IBlockTimeTable {
    id: string;
    date: string;
    time: string;
    wholeDay: boolean;
}
export interface ITeamBlockTimeTable {
    teamCode: string;
    id: string;
    date: string;
    time: string;
    wholeDay: boolean;
}

export interface ISettingsTable {
    type: string;
    settings: Array<Record<string, string>>;
}

export interface IImage {
    id: string;
    description: string;
    path: string;
    url: string;
}

export interface IWorkingDays {
    name: string;
    workingHours: string[];
    workingDay: boolean;
    orderDayOfWeek: number;
}

export interface ITeamWorkingDays {
    teamCode: string;
    name: string;
    workingHours: string[];
    workingDay: boolean;
    orderDayOfWeek: number;
}
export interface IGooglePlace {
    id: string;
    reviews: GoogleReviewInput[];
    user_ratings_total: number;
    rating: string;
}
export interface IGoogleReview {
    author_name: string;
    profile_photo_url: string;
    rating: string;
    relative_time_description: string;
}

export const AWSDATE_FORMAT = 'yyyy-MM-dd';
