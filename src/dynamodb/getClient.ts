import AWS from 'aws-sdk';
import { IClientTable } from './types';
const getClientByMobile = async (mobile?: string): Promise<IClientTable | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'ClientsTable',
            Key: {
                mobile: mobile,
            },
        };
        return (await docClient.get(params).promise()).Item as unknown as IClientTable;
    } catch (error) {
        console.log('error in getClientByMobile:', error);
    }
};

export { getClientByMobile };
