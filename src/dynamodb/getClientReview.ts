import AWS from 'aws-sdk';
import { IClientReviewTable } from './types';
const getClientReview = async (mobile?: string): Promise<IClientReviewTable | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'ClientsReviewTable',
            Key: {
                mobile: mobile,
            },
        };
        return (await docClient.get(params).promise())?.Item as unknown as IClientReviewTable | undefined;
    } catch (error) {
        console.log('error in getClientByMobile:', error);
    }
};

export { getClientReview };
