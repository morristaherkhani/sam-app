import AWS, { AWSError } from 'aws-sdk';
import { AWSDATE_FORMAT, IAppointmentsTable } from './types';
import { addDays, format } from 'date-fns';

const getAppointmentsByDate = async (date: string): Promise<IAppointmentsTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'AppointmentsTable',
            IndexName: 'appointmentAppDateINX',
            KeyConditionExpression: 'appDate = :appDate',
            FilterExpression: 'appointmentStatus IN (:confirmed,:entering,:submitted)',
            ExpressionAttributeValues: {
                ':appDate': date,
                ':confirmed': 'confirmed',
                ':entering': 'entering',
                ':submitted': 'submitted',
            },
        };

        return (await docClient.query(params).promise()).Items as unknown[] as IAppointmentsTable[];
    } catch (error) {
        throw error as AWSError;
    }
};

const getAppointmentsByStatus = async (appointmentStatus: string): Promise<IAppointmentsTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'AppointmentsTable',
            IndexName: 'appointmentStatusINX',
            KeyConditionExpression: 'appointmentStatus = :appointmentStatus',
            ExpressionAttributeValues: {
                ':appointmentStatus': appointmentStatus,
            },
        };
        return (await docClient.query(params).promise()).Items as unknown[] as IAppointmentsTable[];
    } catch (error) {
        throw error as AWSError;
    }
};

const getAppointmentsByDateRange = async (from: string, to: string): Promise<IAppointmentsTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        let fromDate = new Date(from);
        const toDate = new Date(to);
        const appointmentsPromises = [];
        while (fromDate <= toDate) {
            const params = {
                TableName: 'AppointmentsTable',
                IndexName: 'appointmentAppDateINX',
                KeyConditionExpression: 'appDate = :appDate',
                FilterExpression: 'appointmentStatus IN (:confirmed,:entering,:submitted)',
                ExpressionAttributeValues: {
                    ':appDate': format(fromDate, AWSDATE_FORMAT),
                    ':confirmed': 'confirmed',
                    ':entering': 'entering',
                    ':submitted': 'submitted',
                },
            };
            appointmentsPromises.push(
                (await docClient.query(params).promise()).Items as unknown[] as IAppointmentsTable[],
            );
            fromDate = addDays(fromDate, 1);
        }

        return (await Promise.all(appointmentsPromises)).reduce((result, apps) => [...result, ...apps]);
    } catch (error) {
        throw error as AWSError;
    }
};

const getAppointmentById = async (id: string): Promise<IAppointmentsTable | undefined> => {
    //get by hask key
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'AppointmentsTable',
            Key: {
                id: id,
            },
        };
        return (await docClient.get(params).promise()).Item as unknown as IAppointmentsTable;
    } catch (error) {
        console.log('error in getAppointmentById:', error);
    }
};

const getAppointmentByMobile = async (mobile: string): Promise<IAppointmentsTable[] | undefined> => {
    //get by hask key
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'AppointmentsTable',
            IndexName: 'appointmentMobileINX',
            KeyConditionExpression: 'mobile = :mobile',
            ScanIndexForward: false,
            FilterExpression: 'appointmentStatus IN (:confirmed,:entering,:submitted)',
            ExpressionAttributeValues: {
                ':mobile': mobile,
                ':confirmed': 'confirmed',
                ':entering': 'entering',
                ':submitted': 'submitted',
            },
        };

        return (await docClient.query(params).promise()).Items as unknown[] as IAppointmentsTable[];
    } catch (error) {
        console.log('error in getAppointmentById:', error);
    }
};

export {
    getAppointmentsByDate,
    getAppointmentById,
    getAppointmentByMobile,
    getAppointmentsByDateRange,
    getAppointmentsByStatus,
};
