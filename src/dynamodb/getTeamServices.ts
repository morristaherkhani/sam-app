import AWS, { AWSError } from 'aws-sdk';
import { ITeamServiceTable } from './types';
const getTeamServices = async (attributesToGet: string[] = []): Promise<ITeamServiceTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'TeamServicesTable',
            AttributesToGet: ['name', 'code', 'displayOrder', ...attributesToGet],
        };

        return ((await docClient.scan(params).promise()).Items as unknown[] as ITeamServiceTable[]).sort(
            (a, b) => a.displayOrder - b.displayOrder,
        );
    } catch (error) {
        throw error as AWSError;
    }
};

export { getTeamServices };
