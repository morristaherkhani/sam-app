import AWS, { AWSError } from 'aws-sdk';
import { ITeamBlockTimeTable } from './types';
const getTeamBlockTimes = async (
    date: string,
    teamCode: string,
): Promise<{ isBlockedWholeDay: boolean; blockTime: ITeamBlockTimeTable[] } | undefined> => {
    const docClient = new AWS.DynamoDB.DocumentClient();
    try {
        const params = {
            TableName: 'TeamBlockTimesTable',
            IndexName: 'teamBlockTimesDateTeamCodeINX',
            KeyConditionExpression: '#teamCode = :teamCode AND #date = :date',
            ExpressionAttributeNames: {
                '#teamCode': 'teamCode',
                '#date': 'date',
            },
            ExpressionAttributeValues: {
                ':teamCode': teamCode,
                ':date': date, // Format depends on your date storage
            },
        };

        const blockTime = (await docClient.query(params).promise()).Items as unknown[] as ITeamBlockTimeTable[];
        return { isBlockedWholeDay: Boolean(blockTime?.find((time) => time.wholeDay)), blockTime };
    } catch (error) {
        throw error as AWSError;
    }
};

export { getTeamBlockTimes };
