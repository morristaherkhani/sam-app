import AWS, { AWSError } from 'aws-sdk';
import { IServiceTable } from './types';
const getServiceByCode = async (code: string, attributesToGet: string[] = []): Promise<IServiceTable | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'ServicesTable',
            Key: {
                code,
            },
            AttributesToGet: ['name', 'code', 'displayOrder', ...attributesToGet],
        };
        return (await docClient.get(params).promise()).Item as unknown as IServiceTable;
    } catch (error) {
        throw error as AWSError;
    }
};

export { getServiceByCode };
