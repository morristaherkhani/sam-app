import AWS, { AWSError } from 'aws-sdk';
import { AWSDATE_FORMAT, ITeamAppointmentsTable } from './types';
import { addDays, format } from 'date-fns';

const getTeamAppointmentsByDate = async (date: string): Promise<ITeamAppointmentsTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'TeamAppointmentsTable',
            IndexName: 'appointmentAppDateINX',
            KeyConditionExpression: 'appDate = :appDate',
            FilterExpression: 'appointmentStatus IN (:confirmed,:entering,:submitted)',
            ExpressionAttributeValues: {
                ':appDate': date,
                ':confirmed': 'confirmed',
                ':entering': 'entering',
                ':submitted': 'submitted',
            },
        };

        return (await docClient.query(params).promise()).Items as unknown[] as ITeamAppointmentsTable[];
    } catch (error) {
        throw error as AWSError;
    }
};

const getTeamAppointmentsByStatus = async (
    appointmentStatus: string,
): Promise<ITeamAppointmentsTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'TeamAppointmentsTable',
            IndexName: 'appointmentStatusINX',
            KeyConditionExpression: 'appointmentStatus = :appointmentStatus',
            ExpressionAttributeValues: {
                ':appointmentStatus': appointmentStatus,
            },
        };
        return (await docClient.query(params).promise()).Items as unknown[] as ITeamAppointmentsTable[];
    } catch (error) {
        throw error as AWSError;
    }
};

const getTeamAppointmentsByDateRangeTeamCode = async (
    from: string,
    to: string,
    teamCode: string,
): Promise<ITeamAppointmentsTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        let fromDate = new Date(from);
        const toDate = new Date(to);
        const appointmentsPromises = [];
        while (fromDate <= toDate) {
            const params = {
                TableName: 'TeamAppointmentsTable',
                IndexName: 'appointmentDateTeamCodeINX',
                KeyConditionExpression: 'appDate = :appDate AND teamCode = :teamCode',
                FilterExpression: 'appointmentStatus IN (:confirmed,:entering,:submitted)',
                ExpressionAttributeValues: {
                    ':appDate': format(fromDate, AWSDATE_FORMAT),
                    ':teamCode': teamCode,
                    ':confirmed': 'confirmed',
                    ':entering': 'entering',
                    ':submitted': 'submitted',
                },
            };
            appointmentsPromises.push(
                (await docClient.query(params).promise()).Items as unknown[] as ITeamAppointmentsTable[],
            );
            fromDate = addDays(fromDate, 1);
        }

        return (await Promise.all(appointmentsPromises)).reduce((result, apps) => [...result, ...apps]);
    } catch (error) {
        throw error as AWSError;
    }
};

const getTeamAppointmentById = async (id: string): Promise<ITeamAppointmentsTable | undefined> => {
    //get by hask key
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'TeamAppointmentsTable',
            Key: {
                id: id,
            },
        };
        return (await docClient.get(params).promise()).Item as unknown as ITeamAppointmentsTable;
    } catch (error) {
        console.log('error in getAppointmentById:', error);
    }
};

const getTeamAppointmentByMobile = async (mobile: string): Promise<ITeamAppointmentsTable[] | undefined> => {
    //get by hask key
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'TeamAppointmentsTable',
            IndexName: 'appointmentMobileINX',
            KeyConditionExpression: 'mobile = :mobile',
            ScanIndexForward: false,
            FilterExpression: 'appointmentStatus IN (:confirmed,:entering,:submitted)',
            ExpressionAttributeValues: {
                ':mobile': mobile,
                ':confirmed': 'confirmed',
                ':entering': 'entering',
                ':submitted': 'submitted',
            },
        };

        return (await docClient.query(params).promise()).Items as unknown[] as ITeamAppointmentsTable[];
    } catch (error) {
        console.log('error in getAppointmentById:', error);
    }
};

const getTeamAppointmentsByDateTeamCode = async (
    date: string,
    teamCode: string,
): Promise<ITeamAppointmentsTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'TeamAppointmentsTable',
            IndexName: 'appointmentDateTeamCodeINX',
            KeyConditionExpression: 'appDate = :appDate and teamCode = :teamCode',
            FilterExpression: 'appointmentStatus IN (:confirmed,:entering,:submitted)',
            ExpressionAttributeValues: {
                ':teamCode': teamCode,
                ':appDate': date,
                ':confirmed': 'confirmed',
                ':entering': 'entering',
                ':submitted': 'submitted',
            },
        };

        return (await docClient.query(params).promise()).Items as unknown[] as ITeamAppointmentsTable[];
    } catch (error) {
        throw error as AWSError;
    }
};

const getTeamAppointmentsByDateRange = async (
    from: string,
    to: string,
): Promise<ITeamAppointmentsTable[] | undefined> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        let fromDate = new Date(from);
        const toDate = new Date(to);
        const appointmentsPromises = [];
        while (fromDate <= toDate) {
            const params = {
                TableName: 'TeamAppointmentsTable',
                IndexName: 'appointmentAppDateINX',
                KeyConditionExpression: 'appDate = :appDate',
                FilterExpression: 'appointmentStatus IN (:confirmed,:entering,:submitted)',
                ExpressionAttributeValues: {
                    ':appDate': format(fromDate, AWSDATE_FORMAT),
                    ':confirmed': 'confirmed',
                    ':entering': 'entering',
                    ':submitted': 'submitted',
                },
            };
            appointmentsPromises.push(
                (await docClient.query(params).promise()).Items as unknown[] as ITeamAppointmentsTable[],
            );
            fromDate = addDays(fromDate, 1);
        }

        return (await Promise.all(appointmentsPromises)).reduce((result, apps) => [...result, ...apps]);
    } catch (error) {
        throw error as AWSError;
    }
};

export {
    getTeamAppointmentsByDate,
    getTeamAppointmentById,
    getTeamAppointmentByMobile,
    getTeamAppointmentsByDateRangeTeamCode,
    getTeamAppointmentsByStatus,
    getTeamAppointmentsByDateTeamCode,
    getTeamAppointmentsByDateRange,
};
