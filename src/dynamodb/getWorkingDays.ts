import AWS, { AWSError } from 'aws-sdk';
import { IWorkingDays } from './types';

const getWorkingDays = async () => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'WorkingDaysTable',
        };
        return (await docClient.scan(params).promise()).Items as unknown[] as IWorkingDays[];
    } catch (error) {
        throw error as AWSError;
    }
};

export { getWorkingDays };
