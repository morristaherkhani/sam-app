import AWS, { AWSError } from 'aws-sdk';
import { IAppointmentsTable } from './types';

const putAppointment = async (appointment: IAppointmentsTable) => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const paramsAppointment = {
            TableName: 'AppointmentsTable',
            Item: appointment,
        };
        await docClient.put(paramsAppointment).promise();
    } catch (error) {
        console.log('error:', error);
        throw error as AWSError;
    }
};

export { putAppointment };
