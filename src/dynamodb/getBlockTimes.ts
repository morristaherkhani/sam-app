import AWS, { AWSError } from 'aws-sdk';
import { IBlockTimeTable } from './types';
const getBlockTimes = async (
    date: string,
): Promise<{ isBlockedWholeDay: boolean; blockTime: IBlockTimeTable[] } | undefined> => {
    const docClient = new AWS.DynamoDB.DocumentClient();
    try {
        const params = {
            TableName: 'BlockTimesTable',
            FilterExpression: '#blockDate = :blockDate',
            ExpressionAttributeNames: {
                '#blockDate': 'date',
            },
            ExpressionAttributeValues: {
                ':blockDate': date,
            },
        };
        const blockTime = (await docClient.scan(params).promise()).Items as unknown[] as IBlockTimeTable[];
        return { isBlockedWholeDay: Boolean(blockTime?.find((time) => time.wholeDay)), blockTime };
    } catch (error) {
        throw error as AWSError;
    }
};

export { getBlockTimes };
