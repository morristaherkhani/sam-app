import AWS, { AWSError } from 'aws-sdk';
import { getClientReview } from './getClientReview';

const putClientReview = async (mobile: string, date: string, isGiven: boolean) => {
    const TableName = 'ClientsReviewTable';
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const existingReview = await getClientReview(mobile);
        if (Boolean(existingReview)) {
            const updateParams = {
                TableName,
                Key: { mobile: mobile },
                UpdateExpression: 'SET sentDates = list_append(sentDates, :newDate), isGiven = :newIsGiven',
                ExpressionAttributeValues: {
                    ':newDate': [date], // date must be an array for list_append to work
                    ':newIsGiven': isGiven,
                },
                ReturnValues: 'UPDATED_NEW',
            };
            const updateResult = await docClient.update(updateParams).promise();
            return {
                mobile,
                isGiven,
                sentDates: updateResult.Attributes?.sentDates,
            };
        } else {
            const putParams = {
                TableName,
                Item: {
                    mobile,
                    isGiven,
                    sentDates: [date],
                },
            };
            await docClient.put(putParams).promise();
            return {
                mobile,
                isGiven: false,
                sentDates: [date],
            };
        }
    } catch (error) {
        throw error as AWSError;
    }
};

export { putClientReview };
