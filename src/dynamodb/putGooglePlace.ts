import AWS, { AWSError } from 'aws-sdk';
import { IGooglePlace } from './types';

const putGooglePlace = async (googlePlace: IGooglePlace) => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const params = {
            TableName: 'GooglePlaceTable',
            Item: googlePlace,
        };
        const result = await docClient.put(params).promise();

        return result;
    } catch (error) {
        throw error as AWSError;
    }
};

export { putGooglePlace };
