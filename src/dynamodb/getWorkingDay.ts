import AWS, { AWSError } from 'aws-sdk';
import { IWorkingDays } from './types';

const getWorkingDay = async (date: string): Promise<IWorkingDays> => {
    try {
        const docClient = new AWS.DynamoDB.DocumentClient();
        const dayOfWeek = new Date(date).toLocaleString('en-au', { weekday: 'long' });
        const params = {
            TableName: 'WorkingDaysTable',
            Key: { name: dayOfWeek },
        };
        return (await docClient.get(params).promise()).Item as unknown as IWorkingDays;
    } catch (error) {
        throw error as AWSError;
    }
};

export { getWorkingDay };
