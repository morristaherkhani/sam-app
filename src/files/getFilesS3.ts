export const S3_BUCKET_NAME = 'lashdiamond-files';
import { S3Client, ListObjectsV2Command } from '@aws-sdk/client-s3';
import { getUrlFromBucket } from '../utils';

const getFilesS3 = async (s3Path: string) => {
    const client = new S3Client();
    const result = await client.send(
        new ListObjectsV2Command({
            Bucket: S3_BUCKET_NAME,
            Prefix: s3Path,
        }),
    );
    return (
        result?.Contents?.filter((x) => x.Size != 0)?.map((x) => ({
            url: getUrlFromBucket(x.Key ?? ''),
            filename: x.Key ?? '',
        })) ?? []
    );
};

export { getFilesS3 };
